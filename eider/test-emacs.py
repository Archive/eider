#!/usr/bin/env python
import sys
import os
sys.path.append ("init-modules")
import CaptiveEmacs
import libglade
from gtk import *
from gnome.ui import *
from gnome.uiconsts import *
import _gnomeui
import signal

# GLOBALS

captive = None
xml = None
mainwin = None

def get_color_name(color_picker):
	(r,g,b,a) = color_picker.get_i8();
	return "#%02x%02x%02x" % (r,g,b)

def get_font_name(font_picker):
	# PyGnome hasa typo gnome => gnoem, so we need to call the lowlevel layer
	return _gnomeui.gnome_font_picker_get_font_name(font_picker._o)

def start_emacs(widget):
	global captive
	foreground = get_color_name(xml.get_widget("foreground-color"))
	background = get_color_name(xml.get_widget("background-color"))
	font = get_font_name(xml.get_widget("font-picker"))
	captive = CaptiveEmacs.Captive(foreground=foreground, background=background, font=font)

def kill_emacs(widget):
	global captive
	if captive:
		captive.send("(kill-emacs)", 0)
		captive = None

def eval_in_emacs(widget):
	global captive, mainwin, xml
	if captive:
		(status,result) = captive.send(eval_entry.get_text(), 1)
		if not status:
 			box = GnomeMessageBox("Error in evaluation:\n" + result, "error",
								  STOCK_BUTTON_OK)
			box.set_parent(mainwin)
			box.set_modal(TRUE)
			box.show()
		else:
			xml.get_widget("result-label").set_text (result)

xml = libglade.GladeXML ("glade/test-emacs.glade");

mainwin = xml.get_widget("MainWindow")
mainwin.connect("destroy", mainquit)
mainwin.set_default_size (400,200)

xml.get_widget("start-button").connect("clicked", start_emacs);
xml.get_widget("kill-button").connect("clicked", kill_emacs);
xml.get_widget("foreground-color").set_i8(0x00,0x00,0x00,0xff);
xml.get_widget("background-color").set_i8(0xaa,0xaa,0xaa,0xff);
xml.get_widget("font-picker").set_font_name("-b&h-lucidatypewriter-medium-r-normal-sans-*-120-*-*-*-*-iso8859-1");

# Work around bug where the above doesn't cause the name to be updated
xml.get_widget("font-picker").set_mode(FONT_PICKER_MODE_PIXMAP)
xml.get_widget("font-picker").set_mode(FONT_PICKER_MODE_FONT_INFO)

eval_entry = xml.get_widget("eval-entry")
eval_entry.connect("activate", eval_in_emacs);
xml.get_widget("eval-button").connect("clicked", eval_in_emacs);

# Local Variables: ***
# tab-width: 4 ***
if __name__ == "__main__":
	  signal.signal(signal.SIGINT, signal.SIG_DFL) # for control-C

mainloop()


