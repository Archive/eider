#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>

#define BUFSIZE 1024

int do_read (int fd, char **buf, int *len, int *max)
{
  int count;

  if (*len + BUFSIZE/2 > *max)
    {
      *max += BUFSIZE;
      if (*buf)
	*buf = realloc (*buf, *max);
      else
	*buf = malloc (*max);
    }

  count = read (fd, *buf + *len, *max - *len);
  if (count == 0)
    return 0;
  if (count < 0)
    {
      fprintf (stderr, "Error reading from %d: %s\n", fd, strerror (errno));
      exit(1);
    }

  *len += count;
  return 1;
}

int do_write (int fd, char **buf, int *len)
{
  int count;
  int total = strlen (*buf) + 1;
  int written = 0;

  while (total - written > 0)
    {
      count = write (fd, *buf + written, total - written);
      if (count < 0)
	{
	  fprintf (stderr, "Error writing to %d: %s\n", fd, strerror (errno));
	  exit(1);
	}

      written += count;
    }

  *len -= written;
  memmove (*buf, *buf + written, *len);
  
  return 1;
}

int main(int argc, char **argv)
{
  int in_fd, out_fd;
  fd_set in_fds, out_fds, except_fds;
  char *inbuf = NULL, *outbuf = NULL;
  int inbuf_len = 0;
  int outbuf_len = 0;
  int inbuf_max = 0;
  int outbuf_max = 0;
  int i;

  if (argc != 3)
    {
      fprintf (stderr, "Usage: captiverserver IN_FD OUTFD\n");
      fprintf (stderr, "This is used by eider to communicate with emacs.\n");
      fprintf (stderr, "You probably don't want to be running this.\n");
      exit(1);
    }

  in_fd = atoi (argv[1]);
  out_fd = atoi (argv[2]);

  while (1)
    {
      int count;
      
      FD_ZERO (&in_fds);
      FD_ZERO (&out_fds);
      FD_ZERO (&except_fds);
  
      FD_SET (0, &in_fds);
      FD_SET (in_fd, &in_fds);
  
      count = select (in_fd + 1, &in_fds, &out_fds, &except_fds, NULL);
      if (count < 0)
	{
	  fprintf (stderr, "Error selecting: %s\n", strerror (errno));
	  exit(1);
	}

      if (FD_ISSET (0, &in_fds))
	{
	  if (!do_read (0, &outbuf, &outbuf_len, &outbuf_max))
	    exit(1);

	  while (1)
	    {
	      for (i = 0; i<outbuf_len; i++)
		if (!outbuf[i])
		  break;

	      if (i == outbuf_len)
		break;

	      if (!do_write (out_fd, &outbuf, &outbuf_len))
		exit(1);
	    }
	}

      if (FD_ISSET (in_fd, &in_fds))
	{
	  if (!do_read (in_fd, &inbuf, &inbuf_len, &inbuf_max))
	    exit(1);

	  while (1)
	    {
	      for (i = 0; i<inbuf_len; i++)
		if (!inbuf[i])
		  break;

	      if (i == inbuf_len)
		break;

	      if (!do_write (1, &inbuf, &inbuf_len))
		exit(1);
	    }
	}
    }
}
