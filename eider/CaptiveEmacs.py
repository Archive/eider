import posix
import os
import re
import sys
import string

class Captive:

	def __init__(self, foreground=None, background=None, font=None):
		(self.inpipe,inpipe_other) = posix.pipe()
		(outpipe_other, self.outpipe) = posix.pipe()

		pid = posix.fork()

		if pid:
			posix.close(inpipe_other)
			posix.close(outpipe_other)
	
			(process,status) = posix.waitpid(pid, 0)
		else:
			posix.close(self.inpipe)
			posix.close(self.outpipe)
		
			pid2 = posix.fork()
	
			if pid2:
				posix._exit(0)


			args = ["emacs",
				"--load", "./captive.el",
				"--eval", "(captive-start " + `outpipe_other` + " "+ `inpipe_other` + ")"]

			xrm_strings = []

			if foreground != None:
				xrm_strings.append("emacs*Foreground:" + foreground)
			if background != None:
				xrm_strings.append("emacs*Background:" + background)
			if font != None:
				xrm_strings.append("emacs*font:" + font)

			if len(xrm_strings) > 0:
				args.append("--xrm")
				args.append(string.join(xrm_strings,"\n"))

			os.execvp ("emacs", args)

		self.seqno = 0
		self.pending = {}
		self.buf = ""
		self.close_cb = None

	def set_close_cb(self, close_cb):
	    self.close_cb = close_cb

	def read(self):
		str = posix.read(self.inpipe, 1024)
		if str == "":
			if self.close_cb:
				self.close_cb()
			else:
				print "Subprocess exited"
				os.exit(1)

		self.buf = self.buf + str
			
		index = string.find(self.buf, "\0")
		if (index >= 0):
			command = self.buf[:index]
			self.buf = self.buf[index+1:]

			match =	re.match("(EVAL|RESULT|ERROR)\s*([0-9]*)\s*\n(.*)", command)
			if match:
				keyword = match.group(1)
				seqno = string.atoi(match.group(2))
				text = match.group(3)

				if keyword == "EVAL":
					result = eval(text)
					posix.write(self.outpipe,"RESULT " + `seqno` + "\n" + `result` + "\0")
				elif keyword == "RESULT":
					if self.pending.has_key(seqno):
						self.pending[seqno] = (1,text)
				else:
					if self.pending.has_key(seqno):
						self.pending[seqno] = (0,text)
			else:
				print "Unexpected command \"" + command + "\"\n"
	
	def get_input_fileno():
		return self.inpipe

	def send(self, command, wait=1):
		seqno = self.seqno
		self.seqno = self.seqno + 1
		posix.write(self.outpipe,"EVAL %d\n" % seqno + command + "\0")
			
		if wait:
			self.pending[seqno] = None
			while self.pending[seqno] == None:
				self.read()
			result = self.pending[seqno]
			del self.pending[seqno]
			return result
		else:
			return None
