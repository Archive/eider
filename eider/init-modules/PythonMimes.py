import os
import string

import libglade
from gnome.ui import *

import EiderActions
import EiderMime
import Eider
import EiderFiles
import EiderUtils


def open_cb (widget, filename):
        editor = Eider.TextEditor.get()
        editor.open_file (filename)

def add_to_project_cb (widget, filename):
	
	(dirname, basename) = os.path.split(filename)
	
	makefile = EiderFiles.Directory(dirname).get_makefile()
	install = EiderFiles.query_install_dir (makefile,
						'Choose installation directory for file \"%s\"' % basename,
						['noinst','python'])

	if install != None:
		varname = install + "_PYTHON"
		value = makefile.get_variable(varname)
		words = string.split (value)
		words.append(basename)
		makefile.set_variable(varname, string.join(words))
		makefile.find_files()
		makefile.write()
		makefile.changed_hook.run()

def file_is_unassigned(filename):
	return EiderFiles.File(filename).get_am_file() == None
		
EiderMime.register_mime_action ("text/x-python",
				EiderActions.Action("Open", open_cb))
EiderMime.register_mime_action ("text/x-python",
				EiderActions.Action("Add as python file",
						    add_to_project_cb,
						    sensitivity_cb = file_is_unassigned))



