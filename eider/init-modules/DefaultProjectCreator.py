import gnome.ui
import libglade
import Project
import Eider
import EiderPreference
import EiderUtils
import ProjectCreator
import os
import sys
import re
from stat import *
from gtk import TRUE
from gtk import FALSE
import gtk

# Pages:
#  1. Language
#  2. Features
#  3. Options

def get_home_dir ():
	if os.environ.has_key('HOME'):
		return os.environ['HOME'] + "/projects"
	return "/tmp/project"

def get_name ():
	user = username = ""
	if os.environ.has_key('USER'):
		user = os.environ['USER']
	if os.environ.has_key('USERNAME'):
		username = os.environ['USERNAME']
	if user == "":
		if username == "":
			return "Unknown user"
		return username
	if username == "":
		return user
	return "%s <%s>" % (username, user)


project_basedir = EiderPreference.Preference ("Project",
					      "basedir",
					      get_home_dir(),
					      "The base location of a project")
project_author = EiderPreference.Preference ("Project",
					     "author",
					     get_name(),
					     "The author of the project")

possible_licenses = ["GNU GPL", "GNU LGPL", "Artistic", "MIT/X-windows", "BSD", "Other"]
project_license = EiderPreference.Preference ("Project",
					      "license",
					      possible_licenses[0],
					      "The default license of the project")



class DefaultProjectCreator (ProjectCreator.ProjectCreator):
        def __init__(self,
		     project = "",
		     license = project_license.get_value(),
		     basedir = project_basedir.get_value(),
		     version = "0.1",
		     authors = project_author.get_value(),
		     system_files = TRUE):
                self.druid = None
                self.xml = None

		self.basedir = basedir
		self.project = project
		self.license = license
		self.version = version
		self.authors = authors
		self.system_files = system_files
		self.check_regex = None

        def get_druid(self, dest_druid):
                self.druid = dest_druid
		xml = libglade.GladeXML(EiderUtils.get_eider_glade_file("default-project-creator.glade"),
                                                     "default_project_creator")
                xml.signal_autoconnect({
                        "std_prepare" : self.std_prepare,
                        "std_next" : self.std_next,
                        "finish_prepare" : self.finish_prepare,
                        "finish_finish" : self.create_project_cb })
		self.project_entry = xml.get_widget("project_entry")
		self.basedir_entry = xml.get_widget("basedir_entry")
		self.version_entry = xml.get_widget("version_entry")
		self.authors_entry = xml.get_widget("authors_entry")
		self.license_optionmenu = xml.get_widget("license_optionmenu")
		self.system_checkbutton = xml.get_widget("system_checkbutton")

		self.project_entry.connect("insert_text", self.insert_text_cb)
		self.std_prepare = 0
                return xml.get_widget("dpc_druid")

        def std_prepare(self, widget, dummy):
		if self.std_prepare == 1:
			return
		self.basedir_entry.gtk_entry().set_text (self.basedir)
		self.version_entry.set_text (self.version)
		self.authors_entry.insert_defaults (self.authors)
		self.system_checkbutton.set_active (self.system_files)
		menu = self.license_optionmenu.get_menu()
		try:
			index = possible_licenses.index(self.license)
		except ValueError:
			index = len (possible_licenses) - 1
		menu.set_active (index)
		self.std_prepare = 1
			
	def std_next(self, widget, dummy):
		self.project = self.project_entry.get_text()
		if (self.project == ""):
			gnome.ui.GnomeWarningDialog("You must specify a project to be created.", widget.get_toplevel())
			self.project_entry.grab_focus()
			return TRUE

		self.version = self.version_entry.get_text()
		if (self.version == ""):
			gnome.ui.GnomeWarningDialog("You must specify a version for your project", widget.get_toplevel())
			self.version_entry.grab_focus()
			return TRUE

		self.authors = self.authors_entry.get_chars(0, -1)
		self.system_files = self.system_checkbutton.get_active()

		menu = self.license_optionmenu.get_menu()
		menu_item = menu.get_active()
		self.license = possible_licenses[menu.children().index(menu_item)]

		# set up basedir
		self.basedir = self.basedir_entry.gtk_entry().get_text()
		self.basedir = os.path.expanduser(self.basedir)
		# FIXME: I guess we should handle the case where they put in ../ and ./
		if self.basedir[0] != "/":
			self.basedir = "/" + self.basedir
		self.basedir = os.path.abspath (self.basedir) # remove all duplicate '/' signs.  ie. /foo////bar->/foo/bar

		try:
			os.stat (os.path.join (self.basedir, self.project))
			gnome.ui.GnomeWarningDialog("The project, %s, already exists.\nPlease select a new project name." %
						    os.path.join (self.basedir, self.project))
			self.project_entry.grab_focus()
			return TRUE
		except OSError: 
			pass

		dirlist = []
		(head, tail) = os.path.split(self.basedir)
		while head != "/":
			dirlist.append (tail)
			(head, tail) = os.path.split(head)

		dirlist.append (tail)
		dirlist.reverse()
		tempdir = "/"
		can_access_parent = os.access(tempdir, os.R_OK|os.W_OK|os.X_OK)
		i = 0
		for dirptr in dirlist:
			tempdir = os.path.join (tempdir, dirptr)

			# why the stat?  We want to know two things; does the file exist, and is it a file.
			# This saves us from making two extraneous calls.  I suppose I could extract the access
			# stuff from the stat as well, but I'm lazy.
			try:
				dirstat = os.stat(tempdir)
			except OSError:
				break
			if S_ISDIR (dirstat[ST_MODE]):
				if os.access(tempdir, os.R_OK|os.W_OK|os.X_OK):
					can_access_parent = 1
				else:
					can_access_parent = 0
			else:
				gnome.ui.GnomeWarningDialog("Cannot create directory %s\nThere is a file in the way.\n\nPlease change the Base Directory and try again." % tempdir, widget.get_toplevel())
				self.basedir_entry.grab_focus ()
				return TRUE
			i=i+1
		self.newdirbase = ""
		(self.newdirbase, dummy) = os.path.split (tempdir)
		self.newdirlist = dirlist[i:]
		
		if not can_access_parent:
			gnome.ui.GnomeWarningDialog("Insufficient permissions to write in directory %s\n\nPlease change the Base Directory and try again." % tempdir, widget.get_toplevel())
			self.basedir_entry.grab_focus ()
			return TRUE

			
		return FALSE

        def finish_prepare(self, widget, dummy):
		text = "Creating project " + self.project
		text = text + "\n\nBase directory: " + self.basedir
		text = text + "\nLicense: " + self.license
		if self.system_files:
			text = text + "\nGNU Files: System files used"
		else:
			text = text + "\nGNU Files: System files copied locally"
		dirtext = self.newdirbase
		if len (self.newdirlist) == 1:
			text = text + "\nThe following directory will be created:"
		elif len (self.newdirlist) > 1:
			text = text + "\nThe following directories will be created:"
		for i in self.newdirlist:
			dirtext = os.path.join (dirtext, i)
			text = text + "\n         " + dirtext

		text = text + "\n\nPress \"Finish\" to finalize the project." 
		widget.set_text(text)

	def create_project_cb(self, widget, dummy):
		self.create_project ()
		widget.get_toplevel().destroy()

	def create_project(self):
		if self.project == "":
			##FIXME: go through project1->n 'til we find a valid one.
			project = "project"
		if self.version == "":
			self.version = "0.1"
		project_path = os.path.join (self.basedir, self.project)
		os.makedirs (project_path)
		makefile_am = open (os.path.join (project_path, "Makefile.am"), "w+")
		makefile_am.close()
		NEWS = open (os.path.join (project_path, "NEWS"), "w+")
		NEWS.close()
		ChangeLog = open (os.path.join (project_path, "ChangeLog"), "w+")
		ChangeLog.close()
		README = open (os.path.join (project_path, "README"), "w+")
		README.close()
		configure_in = open (os.path.join (project_path, "configure.in"), "w+")
		configure_in.write ("AC_INIT()\n\nAM_INIT_AUTOMAKE(%s, %s)\n\nAC_OUTPUT([\nMakefile\n])\n" % (self.project, self.version))
		configure_in.close()
		if self.authors != "":
			AUTHORS = open (os.path.join (project_path, "AUTHORS"), "w+")
			AUTHORS.write (self.authors)
			AUTHORS.close()
		if self.license != "GNU GPL":
			##FIXME deal with the license
			COPYING = open (os.path.join (project_path, "COPYING"), "w+")
			COPYING.write ("licensed under the " + self.license)
			COPYING.close()
		if self.system_files:
			flags = "-a"
		else:
			flags = "-a -c"

		childpid = os.fork()
		if (not childpid):
			os.close(1)
			os.close(2)
			os.chdir (os.path.join (self.basedir, self.project))
			os.execvp("automake", ["automake", flags])
			sys.exit(1)
		
	def insert_text_cb(self, widget, text, length, position):
		if self.check_regex == None:
			self.check_regex = re.compile("[A-Za-z_1-9\.\-]+$")
		if not self.check_regex.match(text[:length]):
			widget.emit_stop_by_name("insert_text")



Eider.project_creators.register(DefaultProjectCreator,
				"Default Project",
                                """Create a generic project.  This makes no assumptions about your project, but rather just creates the bare minimum necessary.""",
                                EiderUtils.get_eider_pixmap_file ("default-project-icon.png"))


