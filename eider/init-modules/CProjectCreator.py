import Eider
import EiderUtils
import gnome.ui
import libglade
from DefaultProjectCreator import *

from gtk import TRUE
from gtk import FALSE
import gtk

class CProjectCreator (DefaultProjectCreator):
        def __init__ (self):
                DefaultProjectCreator.__init__(self)

        def get_druid (self, dest_druid):
                return DefaultProjectCreator.get_druid (self, dest_druid)

        def create_project (self):
                DefaultProjectCreator.create_project(self)

Eider.project_creators.register(CProjectCreator,
                                "C/C++ Project",
                                """Create a C/C++ project.""",
                                EiderUtils.get_eider_pixmap_file ("c-project-icon.png"))
