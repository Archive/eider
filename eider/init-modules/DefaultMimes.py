import EiderMime
import EiderActions
import Eider
import EiderFiles

def open_cb (widget, filename):
        editor = Eider.TextEditor.get()
        editor.open_file (filename)


EiderMime.register_mime_action ("text/x-c",
				EiderActions.Action("Open", open_cb))



