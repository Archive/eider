import Eider
import EiderUtils
import gnome.ui
import libglade
from DefaultProjectCreator import DefaultProjectCreator
from CProjectCreator import CProjectCreator

from gtk import TRUE
from gtk import FALSE
import gtk

class GnomeProjectCreator (CProjectCreator):
        def __init__ (self):
                CProjectCreator.__init__(self)

        def get_druid (self, dest_druid):
                xml = libglade.GladeXML(EiderUtils.get_eider_glade_file("gnome-project-creator.glade"),
                                        "druid")
                xml.signal_autoconnect({
                        "gnome_options_next" : self.gnome_options_next,
                        "entry_page_prepare" : self.entry_page_prepare
                        })
                druid_pages = CProjectCreator.get_druid (self, dest_druid)
                druid = xml.get_widget ("druid")
                #we want to add our own 'finish' page
                druid_pages.remove (druid_pages.children()[-1])
                for I in druid.children():
                        druid.remove(I)
                        druid_pages.append_page(I)
                        I.show()

                
                return druid_pages

        def gnome_options_next(self, widget, dummy):
                pass

        def entry_page_prepare(self, widget, dummy):
                pass

        def create_project (self):
                CProjectCreator.create_project(self)


Eider.project_creators.register(GnomeProjectCreator,
                                "GNOME Project",
                                """Create a new C based GNOME project.""",
                                EiderUtils.get_eider_pixmap_file ("gnome-project-icon.png"))
