import os
import re
import sys
import string
import gnome
from gtk import *
from gtkhtml import *
import cgi # for "escape" 
from EiderUtils import *
import Eider

if __name__ == "__main__":
    sys.path.append("../modules")

from Compile import *

# This class allows you to plug a different style of HTML formatting
# (and different clicked-on-a-link actions) into the default compile
# output display.

class DefaultHTMLGenerator:

    # Possible GNU-compliant error message formats:
    #   filename:lineno:
    #   filename:lineno:column:
    #   progname:filename:lineno:
    #   progname:filename:lineno:column:
    #   progname:
    #
    # where any of them are followed by an error message
    
    regexp_filename_lineno = re.compile("([-\.\w]+):(\d+):\s+(.*)")
    regexp_filename_lineno_column = re.compile("([-\.\w]+):(\d+):(\d+):\s+(.*)")
    regexp_progname_filename_lineno = re.compile("([-\.\w]+):([-\.\w]+):(\d+):\s+(.*)")
    regexp_progname_filename_lineno_column = re.compile("([-\.\w]+):([-\.\w]+):(\d+):(\d+):\s+(.*)")
    regexp_progname = re.compile("([-\.\w]+):\s+(.*)")
    
    # Make output, as in make[1]: Entering directory blah
    regexp_progname_jobno = re.compile("([-\.\w]+)\[(\d+)\]:\s+(.*)")

    regexp_filename_lineno_url = re.compile("(/[-/\.\w]+)/(\d+)$")

    regexp_make_directory_change = re.compile("make\[\d+\]:\s+(Entering|Leaving) directory `([-\w_./]+)'")
    
    def __init__(self):
        self.dir_stack = [""]
        return None

    def make_html_header(self, command, dir):
        self.dir_stack.append(dir)
        esc_command = cgi.escape(command)
        esc_dir = cgi.escape(dir)
        html = '<!DOCTYPE html PUBLIC "-//IETF//DTD HTML 2.0//EN">\n<HTML>\n<HEAD>\n<TITLE>Compilation Output for `' + esc_command + '</TITLE>\n</HEAD>\n<BODY BGCOLOR="#FFFFFF" TEXT="#000000" LINK="#1F00FF" ALINK="#FF0000" VLINK="#9900DD">\n'
        html = html + '<h2>Output from: <i>' + esc_command + '</i> in ' + esc_dir + '</h2>'
        return html

    def make_html_footer(self):
        html = "</BODY>\n</HTML>\n"
        return html

    def make_html(self, buffer):
        html = ""
        
        for line in buffer:
            # See if we need to change the directory that filenames
            # are relative to
            
            mo = self.regexp_make_directory_change.match(line)

            if mo != None:
                (action, dir) = mo.groups()
                dir = cgi.escape(dir, TRUE)
                if action == "Entering":
                    self.dir_stack.append(cgi.escape(dir, TRUE))
                elif action == "Leaving":
                    # Try not to get confused if the entering/leaving
                    # don't alternate properly (for make -j2 we need to
                    # keep up with the job numbers I guess)
                    i = len(self.dir_stack)
                    while i > 0:
                        i = i - 1
                        if self.dir_stack[i] == dir:
                            del self.dir_stack[i]
                            break
            
            # Format the message 
            Done = "done"
            
            filename = None
            lineno = None
            progname = None
            column = None
            message = None
            jobno = None
            message_only = FALSE

            try:
                mo = self.regexp_filename_lineno.match(line)
                if mo != None:
                    (filename, lineno, message) = mo.groups()
                    raise Done
            
                mo = self.regexp_filename_lineno_column.match(line)
                if mo != None:
                    (filename, lineno, column, message) = mo.groups()
                    raise Done

                mo = self.regexp_progname_filename_lineno.match(line)
                if mo != None:
                    (progname, filename, lineno, message) = mo.groups()
                    raise Done

                mo = self.regexp_progname_filename_lineno_column.match(line)
                if mo != None:
                    (progname, filename, lineno, column, message) = mo.groups()
                    raise Done

                mo = self.regexp_filename_lineno.match(line)
                if mo != None:
                    (filename, lineno, message) = mo.groups()
                    raise Done

                mo = self.regexp_progname_jobno.match(line)
                if mo != None:
                    (progname, jobno, message) = mo.groups()
                    raise Done

                mo = self.regexp_progname.match(line)
                if mo != None:
                    (progname, message) = mo.groups()
                    raise Done

                # fallback
                message_only = TRUE
                message = line

            except Done:
                pass

            # Means the regexps didn't really get all the matches
            # i.e. we don't have all the expected groups
            except ValueError:
                print mo.groups() # debug
                pass

            # escape our stuff 

            attr_filename = None
            if (filename != None):
                attr_filename = cgi.escape(filename, TRUE) # escape quotes too
                filename = cgi.escape(filename)

            if (lineno != None):
                lineno = cgi.escape(lineno)
            if (progname != None):
                progname = cgi.escape(progname)
            if (column != None):
                column = cgi.escape(column)
            if (message != None):
                message = cgi.escape(message)
            if (jobno != None):
                jobno = cgi.escape(jobno)

            # Slap in the HTML tags

            link = ""
            formatted = ""

            # if we have file and line, or just file, make a magic link
            if filename != None or (filename != None and lineno != None):
                if len(self.dir_stack) > 0:
                    full_filename = os.path.join(self.dir_stack[-1], attr_filename)
                else:
                    full_filename = attr_filename
                link = '<A HREF="' + full_filename
                if lineno != None:
                    link = link + '/' + lineno
                else:
                    link = link + ':0' # go to line 0
#                link = link + '"><IMG WIDTH="48" HEIGHT="48" SRC="errorimage"></A>  '
                link = link + '"><b>Goto error</b></A>  '
            
            if progname != None:
                if jobno != None:
                    formatted = '<i>' + progname + '</i><font color="#AAAAAA">[' + jobno + ']</font>:'
                else:
                    formatted = '<i>' + progname + '</i>:'
                
            if filename != None:
                formatted = formatted + '<b>' + filename + '</b>:'

            if lineno != None:
                formatted = formatted + '<font color="red">' + lineno + '</font>:'

            if column != None:
                formatted = formatted + '<font color="blue">' + lineno + '</font>:'

            if message != None:
                if message_only:
                    formatted = formatted + '<font color="purple"> ' + message + '</font>'
                else:
                    formatted = formatted + '<font color="black"> ' + message + '</font>'
            
            formatted = '<TT>' + formatted + '</TT>\n'
            html = html + "<BR>" + formatted + "  " + link

        return html

    def resolve_url(self, url):
        if url == "/errorimage":
            imagefile = None
            try:
                imagefile = open(get_gnome_pixmap_file("gnome-error.png"), 'r')
            except OSError:
                pass
            
            if imagefile != None:
                data = imagefile.read()
                return data
            else:
                print "failed to load image file"
        else:
            print "url: %s" % url

        return None
        

    def url_clicked(self, url):
        print "clicked: %s" % url
        
        filename = None
        lineno = None
        mo = self.regexp_filename_lineno_url.match(url)
        if mo != None:
            (filename, lineno) = mo.groups()

            if filename != None:
                editor = Eider.TextEditor.get()
                editor.open_file(filename, string.atoi(lineno)) # OK if lineno is None
            else:
                print "didn't understand URL: %s" % url
        else:
            print "didn't understand URL: %s" % url
                
        return None

class DefaultCompile (Compile):

    def __init__(self):
        Compile.__init__(self)
        
        # GUI
        self.window = GtkWindow(WINDOW_TOPLEVEL)
        self.window.connect("delete_event", self.window_delete)
        self.window.set_title("Eider Compile Window")
        self.window.set_default_size(475, 325)
        self.main_box = GtkVBox()
        self.main_box.connect("destroy", self.widget_destroy)
        self.window.add(self.main_box)

        self.html_widget = GtkHTML()

        scrolled_window = GtkScrolledWindow()
        scrolled_window.set_policy(POLICY_AUTOMATIC, POLICY_AUTOMATIC)

        self.main_box.pack_start(scrolled_window, TRUE, TRUE, 0)
        scrolled_window.add(self.html_widget)

        self.html_widget.connect("url_requested", self.request_url)
        self.html_widget.connect("link_clicked", self.link_clicked)

        self.html_widget.realize()

        # other stuff

        self.command = ""
        self.command_pwd = "/"

        self.web_page = ""

        self.child_pid = 0

        self.html_generator = DefaultHTMLGenerator()
        
        self.reset()

    def reset(self):
        if self.child_pid > 0:
            self.stop()

        self.child_pid = 0
        self.read_output_tag = 0
        self.read_notify_tag = 0
        self.exit_status = 0
        self.buffer = []
        self.last_line_htmlized = 0
        self.html_handle = None
        self.current_output_line = ""
        self.current_error_line = ""
        self.web_page = ""
        self.output_from_child = -1;
        self.notify_from_child = -1;

    def set_command(self, command):
        self.command = command

    def get_command(self):
        return self.command

    def set_compile_pwd(self, dir):
        self.command_pwd = dir

    def get_compile_pwd(self):
        return self.command_pwd

    def get_output(self):
        return self.buffer

    def get_html(self):
        return self.web_page

    def get_exit_status(self):
        return self.exit_status

    def is_running(self):
        return self.child_pid > 0

    def set_html_generator(self, generator):
        self.html_generator = generator

    def run(self):
        (self.output_from_child, child_output_pipe) = os.pipe()
        
        # avoid fooling with SIGCHLD
        (self.notify_from_child, child_notify_pipe) = os.pipe()

        self.child_pid = os.fork()

        if self.child_pid:
            # Parent
            pass
        else:
            # Child
            os.close(self.output_from_child)
            os.close(self.notify_from_child)

            os.close(1);
            os.close(2);

            try:
                os.dup2(child_output_pipe, 1)
            except OSError, (errno, strerror):
                sys.stdout.write("dup2 failed: %s" % strerror)
                
            try:
                os.dup2(child_output_pipe, 2)
            except OSError, (errno, strerror):
                sys.stdout.write("dup2 failed: %s" % strerror)

            pid = os.fork()

            if pid:
                # Parent again, wait on child to return exit code
                os.close(child_output_pipe)

                (process,status) = os.waitpid(pid, 0)

                # Write a byte
                os.write(child_notify_pipe, "g")
                os.close(child_notify_pipe)

                os._exit(status)
                
            else:
                # child compile process

                os.chdir(self.command_pwd)

                # here we really want gnome_util_user_shell()
                argv = ["/bin/bash", "-c", self.command]

                try:
                    os.execv(argv[0], argv)
                except OSError, (errno, strerror):
                    sys.stdout.write("exec failed: %s" % strerror)
                
                os._exit(1)

        # Parent
        os.close(child_output_pipe)
        os.close(child_notify_pipe)

        self.read_output_tag = input_add(self.output_from_child,
                                         GDK.INPUT_READ, self.read_child_stdout)
        
        self.read_notify_tag = input_add(self.notify_from_child,
                                         GDK.INPUT_READ, self.read_child_notify)

        self.web_page = self.html_generator.make_html_header(self.command,
                                                             self.command_pwd)
        self.html_handle = self.html_widget.begin("")
        self.html_widget.write(self.html_handle, self.web_page)
        self.html_widget.parse()
                
        self.window.show_all()

        self.get_run_hook().run(self)

    def read_child(self, fd, vector_of_lines, leftover_chars):
        while 1:
            str = os.read(fd, 1024)

            print str
        
            if str == "":
                return None
            else:
                lines = string.split(str, "\n")                              

                num_lines = len(lines)

                if num_lines == 0:
                    pass
                elif num_lines == 1:
                    leftover_chars = leftover_chars + lines[0]
                    if leftover_chars != "" and leftover_chars[-1] == '\n':
                        vector_of_lines.append(leftover_chars)
                        leftover_chars = ""
                else:
                    leftover_chars = leftover_chars + lines[0]
                    vector_of_lines.append(leftover_chars)

                    for line in lines[1:-1]:
                        vector_of_lines.append(line)
                    
                    leftover_chars = lines[-1]
                    if leftover_chars != "" and leftover_chars[-1] == '\n':
                        vector_of_lines.append(leftover_chars)
                        leftover_chars = ""

    def stuff_html(self):
        html = self.html_generator.make_html(self.buffer[self.last_line_htmlized:])
        if html != None and html != "":
            self.web_page = self.web_page + html
            self.html_widget.write(self.html_handle, html)
            self.last_line_htmlized = len(self.buffer) - 1 # not sure about this
            self.html_widget.parse()
            
    def read_child_stdout(self, source, condition):
        self.read_child(self.output_from_child,
                        self.buffer,
                        self.current_output_line)
        self.stuff_html()

    def read_child_notify(self, source, condition):
        str = os.read(self.notify_from_child, 1)
        self.stop()

    def debug_dump_buffers(self):
        print "DEBUG:"
        for line in self.buffer:
            print "  output: %s" % line
        if self.current_output_line != "":
            print "  output leftover: %s" % self.current_output_line
        if self.current_error_line != "":
            print "  error leftover: %s" % self.current_error_line

    def stop(self):
        # nothing to do, no child running
        if self.child_pid == 0:
            return None

        # Try to get the exit status
        process = None
        status = None
        try:
            (process,status) = os.waitpid(self.child_pid, os.WNOHANG)
        except OSError, (errno, strerror):
            pass

        # if the child hasn't exited yet, then we want to kill it.
        if process != None and process == 0:
             try:
                 os.kill(self.child_pid, signal.SIGTERM)
             except OSError, (errno, strerror):
                 pass

             # Try to get the exit status again
             try:
                 (process,status) = os.waitpid(self.child_pid, os.WNOHANG)
             except OSError, (errno, strerror):
                 pass

             # If the child still hasn't existed, let's try killing it more
             # brutally (though there's a race condition, the SIGTERM may
             # not have taken effect before the waitpid())
             if process != None and process == 0:
                 try:
                     os.kill(self.child_pid, signal.SIGKILL)
                 except OSError:
                     pass

                 # a blocking waitpid() finally, so we are sure to get
                 # a status
                 try:
                     (process,status) = os.waitpid(self.child_pid, 0)
                 except OSError:
                     pass

        if status != None:
            if os.WIFEXITED(status):
                self.exit_status = os.WEXITSTATUS(status)
            elif os.WIFSIGNALED(status):
                self.exit_status = 1
            else:
                self.exit_status = 0 # shouldn't happen though
        else:
            self.exit_status = 0     # shouldn't happen

        # Remove input functions
        input_remove(self.read_output_tag)
        input_remove(self.read_notify_tag)

        os.close(self.output_from_child)
        os.close(self.notify_from_child)

        self.output_from_child = -1;
        self.notify_from_child = -1;

        self.read_output_tag = 0
        self.read_notify_tag = 0

        self.child_pid = 0

        footer = self.html_generator.make_html_footer()
        self.web_page = self.web_page + footer
        self.html_widget.write(self.html_handle, footer)

        self.html_widget.end(self.html_handle, HTML_STREAM_OK)

        self.html_handle = None # end() destroys the handle

        self.html_widget.parse()

        ## Here is a goofy hack to work around GtkHTML
        handle = self.html_widget.begin("")
        self.html_widget.write(handle, self.web_page)
        self.html_widget.end(handle, HTML_STREAM_OK)
        self.html_widget.parse()

        self.get_stop_hook().run(self)
        
        return None

    def request_url(self, html, url, handle):
        data = self.html_generator.resolve_url(url);

        # Calling end() seems to result in a segfault
        if data == None:
            #            self.html_widget.end(handle, HTML_STREAM_ERROR)
            pass
        else:
            self.html_widget.write(handle, data)
            #            self.html_widget.end(handle, HTML_STREAM_OK)
            

    def link_clicked(self, widget, url):
        self.html_generator.url_clicked(url)
        
    def get_widget(self):
        # May eventually return a non-toplevel to be put somewhere else
        return None

    def widget_destroy(self, *args):
        self.window = None
        self.main_box = None
        self.stop()

    def window_delete(self, *args):
        return FALSE  # allow default destroy to happen

if __name__ == "__main__":
    import signal
    signal.signal(signal.SIGINT, signal.SIG_DFL) # for control-C
    c = DefaultCompile()
    c.set_command("make")
    c.set_compile_pwd("/cvs/gnome-cvs/gconf/")
    c.run()
    mainloop()

import Eider

Eider.Compile.register(DefaultCompile, "Compile window", "Default compile output display window") 

