import re
import Eider
import EiderPreference
import TextEditor
import CaptiveEmacs
import types

# For input_add
import gtk;
import GDK;

fore_pref = EiderPreference.Preference ("Emacs",
					"foreground",
					"gray75",
					"Text foreground color")

back_pref = EiderPreference.Preference ("Emacs",
					"background",
					"#000020",
					"Text background color")

font_pref = EiderPreference.Preference ("Emacs",
					"font",
					"lucidasanstypewriter-12",
					"Text font")

class Emacs (TextEditor.TextEditor):
	def __init__ (self):
		TextEditor.TextEditor.__init__ (self)
		self.captive = None

	def ensure_emacs (self):
		if self.captive == None:
			self.captive = CaptiveEmacs.Captive (fore_pref.get_value (),
							     back_pref.get_value (),
							     font_pref.get_value (),
							     globals())
			self.captive_input = gtk.input_add (self.captive.get_input_fileno(), GDK.INPUT_READ,
							    lambda source, condition, self=self: self.captive.read())
			def close_cb (self=self):
				self.captive = None
				gtk.input_remove (self.captive_input)
				self.captive_input = 0
				
			self.captive.set_close_cb (close_cb)

	def open_file (self, filename, line_number = None):
		# FIXME: quote the filename
		self.ensure_emacs ()

		result = self.captive.send ("(find-file \"" + filename + "\")")

		if (line_number != None):
                        if type (line_number) != types.IntType:
                                raise TypeError, "line number must be an integer"
			assert (line_number > 0)
                        print "(goto-line " + `line_number` + ")"
			result = self.captive.send ("(goto-line " + `line_number` + ")")

	def save_file (self, filename):
		if self.captive == None:
			raise RuntimeError, "Emacs not launched"

		# FIXME: quote the filename
		result = self.captive.send (
			"""(let ((b (get-file-buffer \"%s\")))
			     (if b
			         (save-excursion
				   (set-buffer b)
				   (save-buffer))))""" % filename)

	def get_dirty_files (self):
		if self.captive == None:
			return []

		result = self.captive.send ("(captive-get-dirty-files)")

		string_regex = re.compile ('"(([^"\\\\]+|\\\\.)*)"')
		return map (lambda tup: tup[0], string_regex.findall (result))

Eider.TextEditor.register (Emacs, "Emacs", "Emacs text editor")

def run_file_changed_hook (file):
	editor = Eider.TextEditor.get()
	editor.get_file_changed_hook().run (file)


def run_file_saved_hook (file):
	editor = Eider.TextEditor.get()
	editor.get_file_saved_hook().run (file)
