from EiderFiles import *
from Automake import *

class ScriptTarget(Target,GUIAMFile):
	def __init__(self, name, installdir, makefile):
		Target.__init__(self, name, installdir, makefile)

	def get_primary(self):
		return "SCRIPTS"

	def set_variable(self, variable, value):
		raise ParseError, "bad variable"

register_primary("SCRIPTS", ScriptTarget)





