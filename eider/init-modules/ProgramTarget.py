from EiderFiles import *
from Automake import *
import string
import libglade

class ProgramProperties(PropertiesPage):
	def __init__(self, file, property_box):
		self.target = file.get_am_file ()
		self.property_box = property_box

		self.xml = libglade.GladeXML(EiderUtils.get_eider_glade_file("program-target.glade"), "program-target-page")

		entry = self.xml.get_widget ("ldflags-entry")
		entry.set_text(self.target.ldflags)
		entry.connect ("changed", lambda widget, self=self: self.property_box.changed())
		
		sources_clist = self.xml.get_widget ("sources-clist")
		for source in string.split (self.target.sources):
			sources_clist.append ([source])
			
		button = self.xml.get_widget ("source-add-button")
		button.connect ("clicked", self.properties_add_source_cb)
		
		button = self.xml.get_widget ("source-remove-button")
		button.connect ("clicked", self.properties_remove_source_cb)

	def get_widget(self):
		return self.xml.get_widget ("program-target-page")
		
	def get_title(self):
		return "Target"
		
	def apply(self):
		self.target.ld_flags = self.xml.get_widget ("ldflags-entry").get_text()
		
		sources_clist = self.xml.get_widget ("sources-clist")

		# This is really ugly, since there seems to be _no_ way
		# of getting the number of rows in the GtkCList() from pygtk :-(
		row = 0
		sources = []
		while 1:
			try:
				source = sources_clist.get_text(row, 0)
				sources.append(source)
				row = row + 1
			except ValueError:
				break

		self.target.sources = join (sources)
			
	def properties_destroy_cb (self, widget):
		self.xml = None
		
	def properties_add_source_cb (self, widget):
		sources_clist = self.xml.get_widget ("sources-clist")

		(dir,file) = os.path.split (self.target.makefile.filename)
		dirobj = Directory(dir)

		source = dirobj.choose_source (filter="(.c(xx|pp|c)?|.h(pp)?)$")

		if source != None:
			sources_clist.append([source])
			self.property_box.changed()

	def properties_remove_source_cb (self, widget):
		sources_clist = self.xml.get_widget ("sources-clist")
		if (len(sources_clist.selection) > 0):
			sources_clist.remove(sources_clist.selection[0])
			self.property_box.changed()
		
class ProgramTarget(Target,GUIAMFile):
	def __init__(self, filename, installdir, makefile):
		Target.__init__(self, filename, installdir, makefile)

		self.ldadd = ""
		self.ldflags = ""
		self.link = ""
		self.sources = ""
		self.dependencies = ""
		self.properties_xml = None

	def get_property_page(self, file, property_box):
		return ProgramProperties (file, property_box)
	
	def get_primary(self):
		return "PROGRAMS"

	def set_variable(self, variable, value):
		if variable == "LDADD":
			self.ldadd = value
		elif variable == "LDFLAGS":
			self.ldflags = value
		elif variable == "LINK":
			self.link = value
		elif variable == "SOURCES":
			self.sources = value

			for name in string.split(value):
				self.makefile.register_source (name, GUISource(name, self))
				
		# automake seems to accept OBJECTS, it isn't documented, so we disalllow it
		#
		#elif variable == "OBJECTS":
		#	pass
		elif variable == "DEPENDENCIES":
			self.dependencies = value
		else:
			raise ParseError, "bad variable %s" % variable

register_primary("PROGRAMS", ProgramTarget)


