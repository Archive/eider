import string

import EiderActions
from EiderFiles import *
from Automake import *

class PythonTarget(Source,Target,GUIAMFile):
	def __init__(self, filename, installdir, makefile):
		Target.__init__(self, filename, installdir, makefile)
		self.makefile.register_source (filename, self)
		
	def get_primary(self):
		return "PYTHON"

	def set_variable(self, variable, value):
		raise ParseError, "bad variable"

	def _remove_cb(self,widget,filename):
		varname = self.installdir + "_" + self.get_primary()
		words = string.split(self.makefile.get_variable (varname))
		
		try:
			index = words.index(self.get_basename())
		except ValueError:
			raise RuntimeError, "Can't find file being removed"
		
		del words[index]
		self.makefile.set_variable(varname, string.join(words))
		self.makefile.find_files()
		self.makefile.write()
		self.makefile.changed_hook.run()

	def get_actions(self):
		actions = GUIAMFile.get_actions(self)
		actions.append(EiderActions.Action("Remove from project", self._remove_cb))
		return actions

register_primary("PYTHON", PythonTarget)





