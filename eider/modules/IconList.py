import HookList
import GDK
import os
import MainWindow

class IconList:
	selected_hook_list = HookList.HookList ("Called when a file is selected.  Passes in a EiderFiles.File")
	unselected_hook_list = HookList.HookList ("Called when a file is unselected.")

	def __init__(self,list):
	        self.list = list

		list.connect ("select_icon", self.select_icon_cb)
		list.connect ("unselect_icon", self.unselect_icon_cb)
		list.connect ("button_press_event", self.button_press_event_cb)
		self.selected_icon = None
		self.directory = None

        def _fill(self):
		def compare_files(a,b):
			name_a = a.get_basename()
			name_b = b.get_basename()

			if name_a < name_b:
				return -1
			elif name_a > name_b:
				return 1
			else:
				return 0
			
		self.list.clear ()
		files = self.directory.get_files()
		files.sort (compare_files)

		for file in files:
			if not os.path.isdir(file.get_filename()) and MainWindow.filter(file):
				icon = file.get_icon()

				if icon != None:
					pos = self.list.append_imlib(icon, file.get_basename())
					self.list.set_icon_data(pos, file)
				else:
					print "icon file failed for %s" % file.get_filename()

	def fill(self,directory):
		if self.directory != None:
			self.directory.get_changed_hook().remove(self.directory_changed_tag)
		self.directory = directory

		self.directory_changed_tag = directory.get_changed_hook().add(self._fill)
		self._fill()
			
	def get_selected_icon(self):
		return self.selected_icon

	def select_icon_cb(self,list,pos,event):
		self.selected_icon = list.get_icon_data(pos)
		if event.type == GDK.BUTTON_PRESS:
			self.selected_hook_list.run(self.selected_icon)

	def unselect_icon_cb(self,list,pos,event):
		filename = list.get_icon_data(pos)
		if self.selected_icon and self.selected_icon == filename:
			self.selected_icon = None
		self.unselected_hook_list.run()

	def button_press_event_cb(self,list,event):
		if (event.button == 3):
			pos = list.get_icon_at (event.x, event.y)
			if pos != -1:
				file = list.get_icon_data(pos)
				menu = file.get_popup_menu()
				menu.popup(None, None, None, event.button, event.time)
				menu.signal_connect("deactivate",
						    lambda widget: widget.destroy)
							
			list.emit_stop_by_name("button_press_event")
			return 1
		elif event.type == GDK._2BUTTON_PRESS:
			pos = list.get_icon_at (event.x, event.y)
			if pos != -1:
				file = list.get_icon_data(pos)
				#open file
			return 1
		else:
			return 0
