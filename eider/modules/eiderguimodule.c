#include <pygtk/pygdkimlib.h>

static PyObject *merge_images(PyObject *self, PyObject *args)
{
  PyObject *sequence, *result;
  int i, j, k, len;
  int width = 0;
  int height = 0;
  GdkImlibImage **images;
  GdkImlibImage *image;
  guchar *data;
  static GdkImlibColor transparent_color = {0xff, 0, 0xff};

  if (!PyArg_ParseTuple(args, "O",&sequence))
    return NULL;

  len = PyObject_Length (sequence);
  if (len < 0)
    return NULL;

  images = g_new (GdkImlibImage *, len);
  
  for (i=0; i<len; i++)
    {
      PyObject *item;
      
      item = PyList_GetItem (sequence, i);
      if (!item)
	return NULL;

      if (!PyGdkImlibImage_Check (item))
	{
	  PyErr_SetString(PyExc_TypeError, "elements of sequence must be of imlib images");
	  return NULL;
	}
      
      images[i] = PyGdkImlibImage_Get(item);

      if (images[i]->rgb_width > width)
	width = images[i]->rgb_width;
      if (images[i]->rgb_height > height)
	height = images[i]->rgb_height;
    }

  data = g_malloc (width * height * 3);
  
  for (j=0; j<height; j++)
    for (k=0; k<width; k++)
      {
	data[3 * (width * j + k)] = 0xff;
	data[3 * (width * j + k) + 1] = 0x0;
	data[3 * (width * j + k) + 2] = 0xff;
      }

  for (i=0; i<len; i++)
    {
      guchar *im_data = images[i]->rgb_data;
      guchar r, g, b;
      int im_width = images[i]->rgb_width;
      int im_height = images[i]->rgb_width;

      int x_offset = width - im_width;
      int y_offset = width - im_width;
      
      for (j=0; j<im_height; j++)
	for (k=0; k<im_width; k++)
	  {
	    r = im_data[3 * (im_width * j + k)];
	    g = im_data[3 * (im_width * j + k) + 1];
	    b = im_data[3 * (im_width * j + k) + 2];

	    if (r != 0xff || g != 0x0 || b != 0xff)
	      {
		data[3 * (width * (j + y_offset) + k + x_offset)] = r;
		data[3 * (width * (j + y_offset) + k + x_offset) + 1] = g;
		data[3 * (width * (j + y_offset) + k + x_offset) + 2] = b;
	      }
	  }
    }

  image = gdk_imlib_create_image_from_data(data, NULL, width, height);
  gdk_imlib_set_image_shape (image, &transparent_color);
  result = PyGdkImlibImage_New(image);
  
  g_free (data);
  g_free (images);
  
  return result;
}

static PyMethodDef eidergui_methods[] = 
{
  { "merge_images", merge_images, 1 },
  { NULL, NULL }
};

void
init_eidergui(void)
{
  Py_InitModule("_eidergui", eidergui_methods);
  init_pygdkimlib();
}
