import sys
import os
import select
import string
import gtkhtml
import urllib
import urlparse
import HookList
import gtk

def exec_with_capture(command, argv, searchPath = 0, stdin = 0):

    (read, write) = os.pipe()

    childpid = os.fork()
    if (not childpid):
	os.dup2(write, 1)

	if stdin:
	    os.dup2(stdin, 0)
	    os.close(stdin)

	if (searchPath):
	    os.execvp(command, argv)
	else:
	    os.execv(command, argv)

	sys.exit(1)

    os.close(write)

    rc = ""
    s = "1"
    while (s):
	select.select([read], [], [])
	s = os.read(read, 1000)
	rc = rc + s

    os.close(read)

    os.waitpid(childpid, 0)

    return rc

def set_inst_prefix (prefix):
    global inst_prefix
    inst_prefix = prefix

def set_inst_datadir (prefix):
    global inst_glade_dir
    global inst_pixmap_dir
    global inst_datadir
    inst_datadir=prefix
    inst_glade_dir=inst_datadir+"/eider/"
    inst_pixmap_dir=inst_datadir+"/pixmaps/eider"

def set_inst_bindir (bindir):
    global inst_bindir
    inst_bindir=bindir

#why this convolution?
#we want to work both installed an uninstalled. (-:
def get_inst_prefix ():
    return inst_prefix

def get_inst_datadir ():
    return inst_datadir

def get_inst_bindir ():
    return inst_bindir

def get_eider_datadir_file (file):
    return inst_datadir+"/eider/"+file

def get_eider_pixmap_file (file):
    return inst_pixmap_dir+"/"+file

def get_eider_glade_file (file):
    return inst_glade_dir+"/"+file

def get_gnome_pixmap_file (file):
    return gnome_pixmap_dir + file

set_inst_prefix("/usr")
gnome_pixmap_dir = string.strip(exec_with_capture("gnome-config", ["gnome-config", "--datadir", "gnomeui"], searchPath = 1)) + "/pixmaps/"

## Makes creating that browser thing easier
class GtkHTMLBrowser:
    def __init__(self):
        self.htmlw = gtkhtml.GtkHTML()
        self.htmlw.connect("link_clicked", self.change_url)
        self.htmlw.connect("url_requested", self.supply_data)
        self.baseurl = None
        self.url_changed_hooks = HookList.HookList()

        hadj = self.htmlw.get_hadjustment()
        vadj = self.htmlw.get_vadjustment()
        self.swin = gtk.GtkScrolledWindow(hadj,vadj)
        self.swin.set_policy(gtk.POLICY_AUTOMATIC, gtk.POLICY_AUTOMATIC)
        self.swin.add(self.htmlw)
        self.swin.show()
        self.htmlw.show()

    def get_url_changed_hooks(self):
        return self.url_changed_hooks

    def get_widget(self):
        return self.swin

    def get_htmlw(self):
        return self.htmlw

    def change_url(self, new_url):
        self.baseurl = urlparse.urljoin(self.baseurl, new_url)
        self.loc.set_text(self.baseurl)
        self.htmlw.begin(self.baseurl)
        self.htmlw.parse()

    def supply_data(self, htmlw, rurl, shandle):
        fullurl = urlparse.urljoin(self.baseurl, rurl)
        (x1, dummy) = urllib.splitquery(fullurl)
        (external_url, dummy) = urllib.splittag(x1)
        fh = None
        try:
            fh = urllib.urlopen(external_url)
            if not fh and hasattr(self, "resolve_url"):
                fh = self.resolve_url(self.htmlw, rurl, shandle)
        except IOError:
            if hasattr(self, "resolve_url"):
                fh = self.resolve_url(self.htmlw, rurl, shandle)

        if not fh:
            return

        buf = fh.read(8192)
        while len(buf) == 8192:
            self.htmlw.write(shandle, buf)
            buf = fh.read(8192)
        self.htmlw.write(shandle, buf)
        self.htmlw.end(shandle, gtkhtml.HTML_STREAM_OK)
        self.htmlw.parse()
        fh.close()

def runMethod(klass, method, *args):
        if args:
                return apply(klass.__dict__[method], args)
        return klass.__dict__[method]()
