from gtk import *

class Action:
    def __init__ (self, name, func, sensitivity_cb = None):
        self.name = name
        self.func = func
        self.sensitivity_cb = sensitivity_cb

def create_menu (action_list, *args):
    menu = GtkMenu()
    for action in action_list:
        if action == None:
            menuitem = GtkMenuItem()
            menuitem.set_sensitive (0)
        else:
            menuitem = GtkMenuItem(action.name)
            apply(menuitem.signal_connect, ("activate", action.func) + args)
            if action.sensitivity_cb != None:
                menuitem.set_sensitive (apply (action.sensitivity_cb, args))
            
        menuitem.show()
        menu.add(menuitem)

    return menu

