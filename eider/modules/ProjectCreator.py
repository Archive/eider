import Eider
import EiderUtils
import GdkImlib
import gnome.ui
import gtk
from gtk import TRUE
from gtk import FALSE
import libglade



class ProjectCreator:
        def __init__(self):
                raise NotImplementedError

        def get_druid(self, dest_druid):
                raise NotImplementedError

	def create_project(self):
		raise NotImplementedError

class ProjectCreatorDialog:
        def __init__(self):
                self.creator = None
		self.currently_selected = ["","","",""]
                xml = libglade.GladeXML(EiderUtils.get_eider_glade_file("new-project-dialog.glade"), "toplevel")
                self.toplevel = xml.get_widget ("toplevel")
                self.druid = xml.get_widget ("druid")
                self.druid.connect("cancel", self.toplevel.destroy)

                self.startpage = xml.get_widget ("startpage")
                self.startpage.connect("prepare", self.start_prepare)
                self.startpage.connect("next", self.start_fwd)
                self.startpage.connect("finish", self.finish_cb)

                #set up the clist
                self.project_clist = xml.get_widget ("project_clist")

                creators = Eider.project_creators.get_creators()
                if not creators:
                        raise TypeError, "No Creators have been found.  No new projects can be made."
                for I in creators:
                        row = [I[1]]
                        rownum = self.project_clist.append(row)
                        self.project_clist.set_row_data(rownum, I)

                label = xml.get_widget ("description_label")
                align = xml.get_widget ("image_alignment")
                self.project_clist.connect("select_row", self.clist_selected, (label, align))
                self.project_clist.select_row (0, 0)

		checkbutton = xml.get_widget ("customize_checkbutton")
		checkbutton.connect ("toggled", self.customize_cb, None)
		checkbutton.set_active (1)
                self.toplevel.show ()

        def clist_selected (self, widget, row, column, event, widgets):
                creator_list = self.project_clist.get_row_data(self.project_clist.selection[0])
		if self.currently_selected[1] == creator_list[1]:
			return
		self.currently_selected = creator_list
                widgets[0].set_text (creator_list[2])
                if creator_list[3] != None:
                        base_icon = GdkImlib.Image(creator_list[3])
			if len (widgets[1].children()) > 0:
				widgets[1].remove (widgets[1].children()[0])
                        if base_icon != None:
                                base_icon.render()
                                icon = base_icon.make_pixmap()
                                icon.show()
                                widgets[1].add(icon)


	def customize_cb(self, widget, dummy):
		self.druid.set_show_finish (not widget.get_active ())

        def start_prepare(self, widget, dummy):
                self.druid.set_buttons_sensitive(FALSE, TRUE, TRUE)
                return FALSE

        def start_fwd(self, widget, dummy):
                creator_list = self.project_clist.get_row_data(self.project_clist.selection[0])
                new_creator = creator_list [0]

                #FIXME, we're comparing an object and it's type
                if isinstance (self.creator, new_creator):
                        return FALSE

		for I in self.druid.children()[1:]:
                        self.druid.remove(I)

                self.creator = new_creator()
                custom_druid = self.creator.get_druid(self.druid)
                if not custom_druid:
                        raise TypeError, "Why do these type of things always have to happen to ME?"

                next_page = custom_druid.children()[0]
                for I in custom_druid.children():
                        custom_druid.remove(I)
                        self.druid.append_page(I)
                        I.show()

                self.druid.set_page(next_page)
                return TRUE


	def finish_cb(self, widget, dummy):
		if self.creator == None:
			new_creator = self.project_clist.get_row_data(self.project_clist.selection[0])[0]
			self.creator = new_creator()
		self.creator.create_project ()
		self.toplevel.destroy ()
