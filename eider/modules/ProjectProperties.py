import Eider
import EiderUtils
import gnome.ui
import gtk
import libglade

class ProjectProperties:
    def __init__(self, project, parent):
        self.project = project
        
        xml = libglade.GladeXML(EiderUtils.get_eider_glade_file("project-properties.glade"), "project-properties-window")

        self.window = xml.get_widget("project-properties-window")

        self.window.set_parent(parent)

        self.window.connect("apply", self.apply_cb)
        self.window.connect("destroy", self.destroy_cb)

        self.name_entry = xml.get_widget("project_name_entry")
        self.name_entry.set_text(self.project.get_project_name())
        self.name_entry.connect("changed", self.changed_cb)

        self.compile_command_entry = xml.get_widget("compile_command_entry")
        self.compile_command_entry.set_text(self.project.get_command('compile', 'project'))
        self.compile_command_entry.connect("changed", self.changed_cb)

        self.autogen_command_entry = xml.get_widget("autogen_command_entry")
        self.autogen_command_entry.set_text(self.project.get_command('autogen', 'project'))
        self.autogen_command_entry.connect("changed", self.changed_cb)

        self.configure_command_entry = xml.get_widget("configure_command_entry")
        self.configure_command_entry.set_text(self.project.get_command('configure', 'project'))
        self.configure_command_entry.connect("changed", self.changed_cb)

        self.full_build_command_entry = xml.get_widget("full_build_command_entry")
        self.full_build_command_entry.set_text(self.project.get_command('full', 'project'))
        self.full_build_command_entry.connect("changed", self.changed_cb)



        self.local_compile_command_entry = xml.get_widget("local_compile_command_entry")
        self.local_compile_command_entry.set_text(self.project.get_command('compile', 'local'))
        self.local_compile_command_entry.connect("changed", self.changed_cb)

        self.local_autogen_command_entry = xml.get_widget("local_autogen_command_entry")
        self.local_autogen_command_entry.set_text(self.project.get_command('autogen', 'local'))
        self.local_autogen_command_entry.connect("changed", self.changed_cb)

        self.local_configure_command_entry = xml.get_widget("local_configure_command_entry")
        self.local_configure_command_entry.set_text(self.project.get_command('configure', 'local'))
        self.local_configure_command_entry.connect("changed", self.changed_cb)

        self.local_full_build_command_entry = xml.get_widget("local_full_build_command_entry")
        self.local_full_build_command_entry.set_text(self.project.get_command('full', 'local'))
        self.local_full_build_command_entry.connect("changed", self.changed_cb)

        
        self.window.show()

    def get_widget(self):
        return self.window

    def destroy_cb(self, widget):
        self.window = None

    def changed_cb(self, widget):
        self.window.changed()

    def apply_cb(self, propbox, page_num):
        if page_num == -1:
            self.project.set_project_name(self.name_entry.get_chars(0,-1))
            self.project.set_command('compile', 'project', self.compile_command_entry.get_chars(0,-1))
            self.project.set_command('configure', 'project', self.configure_command_entry.get_chars(0,-1))
            self.project.set_command('autogen', 'project', self.autogen_command_entry.get_chars(0,-1))
            self.project.set_command('full', 'project', self.full_build_command_entry.get_chars(0,-1))


            self.project.set_command('compile', 'local', self.local_compile_command_entry.get_chars(0,-1))
            self.project.set_command('configure', 'local', self.local_configure_command_entry.get_chars(0,-1))
            self.project.set_command('autogen', 'local', self.local_autogen_command_entry.get_chars(0,-1))
            self.project.set_command('full', 'local', self.local_full_build_command_entry.get_chars(0,-1))
