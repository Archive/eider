def target_creator_default_name():
        raise NotImplementedError

class TargetCreator:
        default_name = target_creator_default_name
        def __init__(self):
                raise NotImplementedError

        def get_druid(self, dest_druid):
                raise NotImplementedError

        def get_preference_window (self):
                raise NotImplementedError

	def create_source(self):
		raise NotImplementedError

class TargetCreatorDialog:
        """Create a druid for generating a new target"""
        def __init__(self):
                self.creator = None
		serl.currently_selected = ["", "", "", ""]
		xml = libglade.GladeXML(EiderUtils.get_eider_glade_file("new-target-dialog.glade"), "toplevel")
		self.toplevel = xml.get_widget ("toplevel")
		self.druid = xml.get_widget ("druid")
		self.druid.connect ("cancel", self.toplevel.destroy)


