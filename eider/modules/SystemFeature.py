import types
import sys

class SystemFeature:
    def __init__ (self, short_name, long_name):
        self.short_name = short_name
        self.long_name = long_name
        self.feature = None
        self.instance = None

    def register (self, klass, short_name, long_name):
        if not (type(klass) is types.ClassType):
            raise TypeError, "Invalid function added to hook"

        self.feature = (klass, short_name, long_name)

    def get (self):
        if self.feature == None:
            return None

        if self.instance == None:
            self.instance = self.feature[0] ()

        return self.instance

    def get_short_name (self):
        if self.feature == None:
            return None
        return self.feature[1]

    def get_long_name (self):
        if self.feature == None:
            return None
        return self.feature[2]
