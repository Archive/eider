import sys

class HookList:
    tag = 0
    
    def __init__(self, docstring = None):
        self.list = []
        if docstring == None:
            self.docstring = ""
        else:
            self.docstring = docstring

    def add(self,function,*args):
        if not callable (function):
            raise TypeError, "Invalid function added to hook."

        HookList.tag = HookList.tag + 1
        self.list.append ( (function, args, HookList.tag) )
        return HookList.tag

    def remove(self,tag):
        for i in range (len(self.list)):
            if self.list[i][2] == tag:
                del self.list[i]
        
    def run(self,*args):
        for hook in self.list:
            try:
                apply (hook[0], hook[1] + args)
            except TypeError, msg:
                sys.stderr.write ("Invalid function added to hook:\n")
                if (self.docstring != ""):
                    sys.stderr.write ("\t%s\n" % self.docstring)
                sys.stderr.write ("Error is:\n\t%s\n" % msg)

    def get_docstring (self):
        return self.docstring

