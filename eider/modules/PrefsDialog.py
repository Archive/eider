import Eider
import EiderUtils
import gnome.ui
import gtk
import libglade

class PrefsDialog:
    def __init__(self, parent):
        xml = libglade.GladeXML(EiderUtils.get_eider_glade_file("eider-preferences.glade"), "preferences-window")

        self.window = xml.get_widget("preferences-window")

        self.window.set_parent(parent)

        self.window.connect("apply", self.apply_cb)
        self.window.connect("destroy", self.destroy_cb)
        
        self.window.show()

    def get_widget(self):
        return self.window

    def destroy_cb(self, widget):
        self.window = None

    def changed_cb(self, widget):
        self.window.changed()

    def apply_cb(self, propbox, page_num):
        if page_num == -1:
            pass






