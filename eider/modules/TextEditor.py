import HookList

class TextEditor:
	def __init__ (self):
		self.file_changed_hook = None
		self.file_saved_hook = None

	def open_file (self, filename, line_number = None):
		raise NotImplementedError

	def save_file (self, filename):
		raise NotImplementedError

	def get_dirty_files (self):
		raise NotImplementedError

	def get_file_changed_hook (self):
		if self.file_changed_hook == None:
			self.file_changed_hook = HookList.HookList("Hook to run when launching the compile")
		return self.file_changed_hook

	def get_file_saved_hook (self):
		if self.file_saved_hook == None:
			self.file_saved_hook = HookList.HookList("Hook to run when launching the compile")
		return self.file_saved_hook

