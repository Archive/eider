#!/bin/sh
MODNAME="$1"
shift

for I in $*; do
file=`basename $I`
grep "^<title><anchor" $I \
| sed -e \
"s/<title><anchor id=\"\(.*\)\">\(.*\)<\/title>.*/\2:${MODNAME}\/${file}\.html#\1/g" \
-e 's/\.sgml//g' \
-e 's/^enum //g' \
-e 's/^struct //g' \
-e 's/^union //g' \
-e 's/ \?():/:/g'
done
