import types

class Preference:
	def __init__ (self, module, name, default, docstring):
		assert (type (docstring) is types.StringType)
		self.default = default
		self.docstring = docstring;

	def get_value (self):
		return getattr (self, 'value', self.default)

	def set_value (self, value):
		self.value = value

	def set_default (self, default):
		self.default = default

	def get_docstring (self):
		return self.docstring

	def get_hook (self):
		if (not hasattr (self, 'hook')):
			self.hook = HookList ("Hook for Preference: " + self.get_docstring ())

		return self.hook
