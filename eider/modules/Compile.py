from HookList import HookList

class Compile:
    def __init__ (self):
        self.stop_hook = None
        self.run_hook = None

    def set_command(self, command):
        raise NotImplementedError

    def get_command(self):
        raise NotImplementedError

    def set_compile_pwd(self, dir):
        raise NotImplementedError

    def get_compile_pwd(self):
        raise NotImplementedError

    def run(self):
        raise NotImplementedError
    
    def stop(self):
        raise NotImplementedError

    def get_exit_status(self):
        raise NotImplementedError

    def is_running(self):
        raise NotImplementedError

    def get_run_hook(self):
        if self.run_hook == None:
            self.run_hook = HookList("Hook to run when launching the compile")
        return self.run_hook

    def get_stop_hook(self):
        if self.stop_hook == None:
            self.stop_hook = HookList("Hook to run when stopping a compile")
        return self.stop_hook
