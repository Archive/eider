import GdkImlib
import _eidergui

def merge_images (image_list):
    """Take a list of GdkImlib.Image, composite them (first image is lowest) and return
       a GdkImlib.Image for the result"""
    if len(image_list) == 1:
        return image_list[0]
    else:
        im =  _eidergui.merge_images (map (lambda image: image._im, image_list))
        return GdkImlib.Image (_obj = im)
