import os
import re
import gnome
import gtk
import gtkhtml
import EiderUtils
import string
import libglade
import urllib
import urlparse
from SystemFeature import *

target_creators = []
project_creators = []
source_creators = []

EiderHelpBrowser = SystemFeature("Help browser", "Default help browser")

# Helpsys-specific functions
dev_baseurl="http://developer.gnome.org/doc/API/"

class HelpBrowser (EiderUtils.GtkHTMLBrowser):
    def __init__(self):
        EiderUtils.GtkHTMLBrowser.__init(self)

        xml = libglade.GladeXML(EiderUtils.get_eider_glade_file("eiderhelp.glade"), "helpwin")
        self.window = xml.get_widget("helpwin")
        self.window.hide()
        self.window.set_usize(400, 400)
        self.ctree = xml.get_widget("help_tree")
        self.hbox = xml.get_widget("help_hbox")
        self.loc = xml.get_widget("help_loc")

        self.hbox.add(self.get_browser_widget())

        self.funcmap = {}
        self.read_tree()

        self.ctree.connect("tree_select_row", self.select_row)
        self.loc.connect("activate", self.loc_activate)
        self.baseurl = ""

        self.window.show_all()

    def loc_activate(self, widget):
        self.change_url(widget.get_text())

    def select_row(self, ctree, node, column):
        url = ctree.node_get_row_data(node)
        if url:
            self.change_url(dev_baseurl + url)

    def read_tree(self):
        try:
            file_list = os.listdir(EiderUtils.get_eider_datadir_file("tags"))
        except OSError:
            print "Couldn't read tags from dir " + EiderUtils.get_eider_datadir_file("help-data")
            return None
        self.ctree.freeze()
        for file in file_list:
            self.read_tree_file(EiderUtils.get_eider_datadir_file("tags")+"/"+file, file)
        self.ctree.thaw()

    def read_tree_file(self, file, basefile):
        re_get_group = re.compile("^(.*)-tags$")
        mo = re_get_group.match(basefile)
        if not mo:
            return
        (grpname) = mo.groups()
        tree_node = self.ctree.insert_node(None, None, grpname, 5,
                                           None, None, None, None, 0)
        fh = open(file)
        sub_sibling = None
        aline = fh.readline()
        while aline:
            while aline[-1:] == '\n' or aline[-1:] == ' ':
                aline = aline[:-1]
            (funcname, url) = string.split(aline, ':')
            url = url
            self.funcmap[funcname] = url
            namearray = [funcname]
            anode = self.ctree.insert_node(tree_node, None, namearray)
            self.ctree.node_set_row_data(anode, url)
            aline = fh.readline()

    def help_for_function(self, funcname):
        self.change_url(dev_baseurl + self.translate_function(funcname) )

    def translate_function(self, funcname):
        return self.funcmap[funcname]
