import gnome.mime
import types
import EiderActions
import EiderFiles
import re
mime_types_initial = {}
mime_types = {}
mime_actions = {}
mime_page_widgets = {}

def register_mime_action (mime_type, action):
        """Registers the action, for mime_type. action is an objectn
        of type EiderActions.Action
	"""
	if not (isinstance(action, EiderActions.Action)):
                raise TypeError, "Invalid action registered to the mime_actions"

	if not mime_actions.has_key(mime_type):
                mime_actions[mime_type] = { action.name: action }
        elif not mime_actions[mime_type].has_key(action.name):
                mime_actions[mime_type][action.name] = action
        else:
                raise Error, "Mime type: " + mime_type + " already has an action: " + action.name + " registered."

def register_mime_property_page (mime_type, target_class):
        """Registers a target class as the property_page creator of
        mime_type.  target_class is expected to inherit from PropertiesPage"""
        if not issubclass(target_class, EiderFiles.PropertiesPage):
                raise TypeError, "Registering wrong type of PropertiesPage widget for mime type: " + mime_type
        if mime_page_widgets.has_key (mime_type):
                raise Error, "Duplicate PropertiesPage widgets for mime type: " + mime_type
        mime_page_widgets[mime_type] = target_class

def register_initial_mime_type (mime_type, regexp):
        """Registers a new mime type.  These are loaded before the system ones,
	overriding them.  This function should be used with caution"""
        if mime_types_initial.has_key (mime_type):
                raise Error, "Duplicate mime_type registered: " + mime_type
        mime_types_initial[mime_type] = regexp

def register_mime_type (mime_type, regexp):
        """Registers a new mime type.  These are loaded after the system ones."""
        if mime_types.has_key (mime_type):
                raise Error, "Duplicate mime_type registered: " + mime_type
        mime_types[mime_type] = regexp

class Mime:
        def __init__ (self, filename):
                self.filename = filename
		self.mime_type = ""
		for mime in mime_types_initial.items():
			if re.search(mime[1], filename):
				self.mime_type = mime[0]
				break
		if self.mime_type == "":
			self.mime_type = gnome.mime.type_or_default_of_file (filename, "")
                if self.mime_type == "":
			for mime in mime_types.items():
				if re.search(mime[1], filename):
					self.mime_type = mime[0]
					break

        def get_filename (self):
                return self.filename

        def get_mime_type (self):
                return self.mime_type

        def get_actions (self):
                if not mime_actions.has_key (self.mime_type):
                        return None
		return map (lambda tup: tup[1], mime_actions[self.mime_type].items())

        def get_property_page (self, file, dialog):
                if not mime_page_widgets.has_key (self.mime_type):
                        return None
                return mime_page_widgets[self.mime_type](file, dialog)

