import os
import xmllib
import Project
from cgi import escape
import sys

SyntaxError = "Syntax error"

TRUE = 1
FALSE = 0

class ProjectXMLLoader (xmllib.XMLParser):
    def __init__ (self):
        xmllib.XMLParser.__init__(self)

        self.elements = { "eiderdoc" : (self.doc_start, self.doc_end),
                          "name" : (self.name_start, self.name_end),
                          "compile_command" : (self.compile_command_start,
                                               self.compile_command_end),
                          "configure_command" : (self.configure_command_start,
                                                 self.configure_command_end),
                          "autogen_command" : (self.autogen_command_start,
                                               self.autogen_command_end),
                          "full_build_command" : (self.full_build_command_start,
                                                  self.full_build_command_end)
                          }

        self.project = None
        self.current_data_handler = None
        self.current_data = ""
        self.current_file = None

    # You need to catch IOError and SyntaxError when using this.
    def load (self, project, filename, is_local):
        if self.project != None:
            print "can't use same XML loader twice"
            return None

        self.project = project
        
        self.loading_local = is_local
        self.current_file = filename
        
        # Let IOError trickle up
        input = open (filename, 'r')
        
        line = input.readline()
        while (line != ""):
            self.feed(line)
            line = input.readline()

        input.close()
        self.close()
        self.current_file = None

    ################ Element handlers 

    def any_end (self):
        self.current_data = ""
        self.current_data_handler = None

    def any_data_handler (self, data):
        self.current_data = self.current_data + data

    def doc_start (self, attributes):
        pass

    def doc_end (self):
        pass

    def name_end (self):
        if self.loading_local:
            print "warning: local project file shouldn't specify project name"
        self.project.set_project_name(self.current_data)
        self.any_end()

    def name_start (self, attributes):
        self.current_data_handler = self.any_data_handler

    def compile_command_end (self):
        if self.loading_local:
            self.project.set_command('compile', 'local', self.current_data)
        else:
            self.project.set_command('compile', 'project', self.current_data)
        self.any_end()

    def compile_command_start (self, attributes):
        self.current_data_handler = self.any_data_handler

    def configure_command_end (self):
        if self.loading_local:
            self.project.set_command('configure', 'local', self.current_data)
        else:
            self.project.set_command('configure', 'project', self.current_data)
        self.any_end()

    def configure_command_start (self, attributes):
        self.current_data_handler = self.any_data_handler

    def autogen_command_end (self):
        if self.loading_local:
            self.project.set_command('autogen', 'local', self.current_data)
        else:
            self.project.set_command('autogen', 'project', self.current_data)
        self.any_end()

    def autogen_command_start (self, attributes):
        self.current_data_handler = self.any_data_handler

    def full_build_command_end (self):
        if self.loading_local:
            self.project.set_command('full', 'local', self.current_data)
        else:
            self.project.set_command('full', 'project', self.current_data)
        self.any_end()

    def full_build_command_start (self, attributes):
        self.current_data_handler = self.any_data_handler

    ################# Other handlers 

    # handle <?xml ...>
    def handle_xml (self, encoding, standalone):
        pass

    # handle <!DOCTYPE >
    def handle_doctype (self, tag, data):
        pass

    # handles contents of a tag
    def handle_data (self, data):
        if self.current_data_handler != None:
            self.current_data_handler(data)
        else:
            pass

    # handles contents of a comment
    def handle_comment (self, comment):
        pass

    # handles cdata
    def handle_cdata (self, data):
        print "warning, XML cdata section, we do not like cdata"
        pass

    # handles <?xml blah?> processing instructions, except for the mandatory one
    # at the top of the document
    def handle_proc (self, data):
        print "warning, don't like XML processing instructions"
        pass

    # handles entity definitions and such
    def handle_special (self, data):
        print "warning, special XML thing (entity probably): %s" % data
        pass
    
    # handle syntax error
    def syntax_error (self, message):
        raise SyntaxError, self.current_file + ': ' + message

    # handle unrecognized tag
    def unknown_starttag (self, tag, attributes):
        print "unknown tag %s in project file %s" % (tag, self.current_file)
        pass

    # handle close of unrecognized tag
    def unknown_endtag (self, tag):
        pass

    # handle unknown character reference
    def unknown_charref (self, ref):
        print "unknown character reference %s in project file %s" % (ref, self.current_file)
        pass

    # handle unknown entity
    def unknown_entityref (self, ref):
        print "unknown entity %s in project file %s" % (ref, self.current_file)
        pass

# Need to catch IOError and SyntaxError here
def load_to (filename, project):
    loader = ProjectXMLLoader()

    # throw up IOError and SyntaxError
    loader.load(project, filename, FALSE)

    loader = ProjectXMLLoader()
    
    loader.load(project, filename + '.local', TRUE)

# Can throw IOError
def save (project, filebase):
    output = open(filebase, 'w')
    
    output.write('<?xml version="1.0"?>\n<!-- Global Eider IDE project file: do not edit by hand. This file should be in CVS or otherwise shared among all project hackers. -->\n<eiderdoc>\n') 
    
    output.write('<name>' + escape(project.get_project_name()) + '</name>\n')

    output.write('<compile_command>' + escape(project.get_command('compile', 'project')) + '</compile_command>\n')
    output.write('<configure_command>' + escape(project.get_command('configure', 'project')) + '</configure_command>\n')

    output.write('<autogen_command>' + escape(project.get_command('autogen', 'project')) + '</autogen_command>\n')

    output.write('<full_build_command>' + escape(project.get_command('full', 'project')) + '</full_build_command>\n')
    
    output.write('</eiderdoc>\n')

    output.close()

    local = open(filebase + '.local', 'w')
    
    local.write('<?xml version="1.0"?>\n<!-- Local Eider IDE project file: do not edit by hand, do not check this one in to CVS -->\n<eiderdoc>\n') 

    local.write('<compile_command>' + escape(project.get_command('compile', 'local')) + '</compile_command>\n')
    local.write('<configure_command>' + escape(project.get_command('configure', 'local')) + '</configure_command>\n')

    local.write('<autogen_command>' + escape(project.get_command('autogen', 'local')) + '</autogen_command>\n')

    local.write('<full_build_command>' + escape(project.get_command('full', 'local')) + '</full_build_command>\n')
    
    local.write('</eiderdoc>\n')

    local.close()

if __name__ == "__main__":
    import signal
    signal.signal(signal.SIGINT, signal.SIG_DFL) # for control-C

    p = Project()

    p.set_project_name("test project")

    print "saving 1"

    try:
        save(p, "blah.eiderinfo")
    except IOError, (errno, strerror):
        print "Failed to save: %s" % strerror

    print "loading 1"

    try:
        p = load("blah.eiderinfo")
    except SyntaxError, message:
        print "Syntax error: %s" % message
    except IOError, (errno, strerror):
        print "Failed to load: %s" % strerror
        
    print "saving 2"
    try:
        save(p, "blah.eiderinfo")
    except IOError, (errno, strerror):
        print "Failed to save: %s" % strerror

    print "loading 2"
    try:
        p = load("blah.eiderinfo")
    except SyntaxError, message:
        print "Syntax error: %s" % message
    except IOError, (errno, strerror):
        print "Failed to load: %s" % strerror

