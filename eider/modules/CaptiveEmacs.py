import os
import re
import sys
import string
import errno
import EiderUtils # For paths

class UnexpectedExit (Exception):
	"Captive emacs exited unexpectedly"

class ElispError (Exception):
	"Error in executed elisp"

class Captive:
	def __init__(self, foreground=None, background=None, font=None, global_scope=None):
		self.global_scope = global_scope

		(self.inpipe,inpipe_other) = os.pipe()
		(outpipe_other, self.outpipe) = os.pipe()
		pid = os.fork()

		if pid:
			os.close(inpipe_other)
			os.close(outpipe_other)

			(process,status) = os.waitpid(pid, 0)
		else:
			os.close(self.inpipe)
			os.close(self.outpipe)

			pid2 = os.fork()

			if pid2:
				os._exit(0)

			elisp_file = EiderUtils.get_eider_datadir_file("captive.el")
			if not os.path.isfile (elisp_file):
				raise RuntimeError, "Cannot find captive.el"

			server_file = EiderUtils.get_inst_bindir() + "/" + "eider-captive-server"
			if not os.path.isfile (server_file):
				raise RuntimeError, "Cannot find eider-captive-server"

			args = ["emacs",
				"-load", elisp_file,
				'--eval', '(captive-start "' + server_file + '" ' + `outpipe_other` + ' ' + `inpipe_other` + ')']

			xrm_strings = []

			if foreground != None:
				xrm_strings.append("emacs*Foreground:" + foreground)
			if background != None:
				xrm_strings.append("emacs*Background:" + background)
			if font != None:
				xrm_strings.append("emacs*font:" + font)

			if len(xrm_strings) > 0:
				args.append("--xrm")
				args.append(string.join(xrm_strings,"\n"))

			os.execvp ("emacs", args);

		self.closed = 0
		self.seqno = 0
		self.pending = {}
		self.buf = ""
		self.close_cb = None

	def __del__(self):
		os.close(self.inpipe)
		os.close(self.outpipe)

	def set_close_cb(self, close_cb):
	    self.close_cb = close_cb

	def read(self):
		if self.closed:
			return
		
		str = os.read(self.inpipe, 1024)
		if str == "":
			self._close()

		self.buf = self.buf + str

		index = string.find(self.buf, "\0")
		if (index >= 0):
			command = self.buf[:index]
			self.buf = self.buf[index+1:]

			match =	re.match("(EVAL|RESULT|ERROR)\s*([0-9]*)\s*\n(.*)", command)
			if match:
				keyword = match.group(1)
				seqno = string.atoi(match.group(2))
				text = match.group(3)

				if keyword == "EVAL":
					if self.global_scope != None:
						result = eval (text, self.global_scope)
					else:
						result = eval (text)
					self._write("RESULT " + `seqno` + "\n" + `result` + "\0")
				elif keyword == "RESULT":
					if self.pending.has_key(seqno):
						self.pending[seqno] = (1,text)
				else:
					if self.pending.has_key(seqno):
						self.pending[seqno] = (0,text)
			else:
				print "Unexpected command \"" + command + "\"\n"

	def get_input_fileno(self):
		return self.inpipe

	def send(self, command, wait=1):
		if self.closed:
			raise UnexpectedExit

		seqno = self.seqno
		self.seqno = self.seqno + 1
		self._write("EVAL %d\n" % seqno + command + "\0")

		if wait:
			self.pending[seqno] = None
			while not self.closed and self.pending[seqno] == None:
				self.read()

			if self.closed:
				raise UnexpectedExit
			(status,result) = self.pending[seqno]
			del self.pending[seqno]

			if not status:
				raise ElispError, result
			else:
				return result
		else:
			return None

	def _close(self):
		if self.close_cb:
			(self.close_cb)()
		else:
			print "Subprocess exited"
			os.exit(1)

		self.closed = 1

	def _write(self, str):
		try:
			while len(str) > 0:
				count = os.write(self.outpipe, str)
				str = str[count:]
				
		except OSError, err:
			if err.errno == errno.EPIPE:
				self._close()
				raise UnexpectedExit
			else:
				raise err

		
