import os
import ProjectParser

TRUE = 1
FALSE = 0

class Project:
        def __init__ (self, name = "", directory = None):
                # must set up defaults first, then do the
                # from-directory init in case we load new
                # values.
                self.name = name

                self.commands = {}

                self.commands['compile'] = ['make', 'make'];
                self.commands['configure'] = ['./configure', './configure'];
                self.commands['autogen'] = ['./autogen.sh', './autogen.sh'];
                self.commands['full'] = ['./autogen.sh && ./configure --enable-maintainer-mode && make', './autogen.sh && ./configure --enable-maintainer-mode && make']

                self.in_load = FALSE
                
                if (directory == None):
                        self.init_from_curdir ()
                else:
                        self.project_dir = directory

        def init_from_curdir (self):
                # this is a hack to look in the curdir for a project.  It's unclear
                # if this is the best behaviour for the default constructor
                
                # Find a first guess for project directory - look for a parent of
                # this directory with a configure.in
		old_dir = os.getcwd()
		project_dir = old_dir
		
		dir = os.getcwd()
		os.chdir(dir)
		while 1:
			if os.path.isfile("configure.in"):
				project_dir = dir
				break
			os.chdir("..")
			new_dir = os.getcwd()
			if new_dir == dir:
				break
			dir = new_dir

		os.chdir(old_dir)

                self.set_project_dir(project_dir)

        def get_project_file (self):
                return self.project_dir + '/' + '.eiderinfo'

        def get_project_dir (self):
                return self.project_dir

        def set_project_dir (self, directory):
                self.project_dir = directory

                try:
                        # flag to avoid saving while loading,
                        # since we pretty much save anytime
                        # there's a change (should eventually
                        # be in an idle function anyway)
                        self.in_load = TRUE
                        ProjectParser.load_to(self.get_project_file(),
                                              self)
                except ProjectParser.SyntaxError, message:
                        print "Syntax error: %s" % message
                except IOError, (errno, strerror):
                        print "Failed to load: %s" % strerror

                self.in_load = FALSE

        def save_project_file (self):
                if self.in_load:
                        return
                try:
                        ProjectParser.save(self, self.get_project_file())
                except IOError, (errno, strerror):
                        GnomeErrorDialog("Failed to save project file: %s" % strerror)

        def get_project_name (self):
                return self.name

        def set_project_name (self, name):
                self.name = name
                self.save_project_file()

        # category = 'compile' 'autogen' 'configure' 'full'
        # where = 'project' (check in to CVS for world)
        #         'local'   (local copy)
        #         'use'     (command to use for actually compiling stuff,
        #                    only allowed in get_command)
        def set_command (self, category, where, command):
                if not self.commands.has_key(category):
                        print "invalid command category: ", category
                        return

                list = self.commands[category]

                if where == 'project':
                        list[0] = command
                elif where == 'local':
                        list[1] = command
                else:
                        print "invalid command scope: ", where

                self.save_project_file()

        def get_command (self, category, where='use'):
                if not self.commands.has_key(category):
                        print "invalid command category: ", category
                        return

                list = self.commands[category]

                if where == 'project':
                        return list[0]
                elif where == 'local':
                        return list[1]
                elif where == 'use':
                        return list[1]
                else:
                        print "invalid command scope: ", where
        


