import os
from SystemFeature import *
from EiderPreference import *
import types

class CreatorList:
	def __init__(self, name):
		self.creators = []
		self.name = name

	def register(self, klass, short_name, long_name, icon_name = None):
		if not (type(klass) is types.ClassType
			and type(short_name) is types.StringType
			and type(long_name) is types.StringType):
			raise TypeError, "Invalid class registered to the target_creators list"
		self.creators.append(klass, short_name, long_name, icon_name)

	def get_creators(self):
		return self.creators

target_creators = CreatorList("Target creators")
project_creators = CreatorList("Project creators")
source_creators = CreatorList("Source creators")

#TextEditor interface defined in TextEditor.py
TextEditor = SystemFeature ("Text Editor", "Default Text Editor")
#Compile interface defined in Compile.py
Compile = SystemFeature ("Compile module", "Compile output display window")
