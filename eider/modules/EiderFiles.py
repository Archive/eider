import os
import re

from gtk import *
from gnome.ui import *
import GdkImlib

import libglade
import Automake
import HookList
import EiderActions
import EiderUtils
import EiderMime
import EiderImages

# There is some pain in the following because python doesn't have weak references.
# Since we want to keep directory => Directory hash and file => File, we wrap
# DirectoryBase and FileBase in Directory and File and do our own refcounting
    
_directories = {}

class DirectoryBase:
    def __init__ (self, dirname):
        self.dirname = dirname
        self.rescan()
        self.refcount = 1
        self.changed_hook = HookList.HookList("Hook to run when directory contents change")
        _directories[self.dirname] = self
        
    def rescan (self):
        self.files = {}

        makefile_file = os.path.join (self.dirname, "Makefile.am")
        if not os.path.isfile (makefile_file):
            self.makefile = None
        else:
            self.makefile = Automake.Makefile (self.dirname)
            self.makefile.parse()
            self.makefile.find_files()
            self.makefile.changed_hook.add(lambda dirname=self.dirname:\
                                              Directory(dirname).get_changed_hook().run())

        for name in os.listdir(self.dirname):
      	    self.files[name] = File(os.path.join (self.dirname,name))

    def get_files(self):
        result = []

        for (name, file) in self.files.items():
            result.append(file)

        return result

    def ref(self):
        self.refcount = self.refcount + 1
        return self

    def unref(self):
        self.refcount = self.refcount - 1
        if (self.refcount == 0):
            del _directories[self.dirname]

    def choose_source(self, filter):
        xml = libglade.GladeXML(EiderUtils.get_eider_glade_file("eider-files.glade"), "choose-source-file")
        dialog = xml.get_widget ("choose-source-file")
        dialog.close_hides(1)
        
        clist = xml.get_widget("sources-clist")

        filter_re = re.compile(filter)
        for file in os.listdir (self.dirname):
            if filter_re.search(file) and not os.path.isdir (os.path.join(self.dirname,file)):
                clist.append([file])

        result = None
        if dialog.run() == 0:           # OK
            if len(clist.selection) > 0:
                result = clist.get_text(clist.selection[0], 0)

        dialog.destroy()
        return result

class Directory:
    def __init__(self,dirname):
        if _directories.has_key(dirname):
            self.base = _directories[dirname].ref()
        else:
            self.base = DirectoryBase(dirname)

    def __del__(self):
	# We need the hasattr check, since we might get destroyed if the constructor fails
        if hasattr (self, 'base'):
		self.base.unref()

    def rescan (self):
        self.base.rescan()

    def get_makefile(self):
        return self.base.makefile

    def get_files(self):
        return self.base.get_files()

    def choose_source(self, filter=""):
        return self.base.choose_source(filter)

    def get_changed_hook(self):
        return self.base.changed_hook
        
_files = {}

class FileBase:
    def __init__ (self, filename):
        self.filename = filename
        self.refcount = 1
        _files[filename] = self

    def get_am_file (self):
        (dirname,base) = os.path.split (self.filename)
        makefile = Directory (dirname).get_makefile()
        if not makefile:
            return None
        
        if  makefile.targets.has_key (base):
            return makefile.targets[base]
        elif makefile.sources.has_key (base):
            return makefile.sources[base]

    def get_mime_type (self):
        if not hasattr (self, 'mime_type'):
            self.mime_type = EiderMime.Mime (self.filename)
        return self.mime_type

    def get_popup_menu (self):
        actions = []
        am_file = self.get_am_file()
        if am_file:
            actions = am_file.get_actions()

        mime_time = self.get_mime_type()
        if mime_time:
            mime_actions = mime_time.get_actions()
            if mime_actions and len(mime_actions) > 0:
                if len(actions) > 0:
                    actions.append(None)
                actions = actions + mime_actions

        if len(actions) > 0:
            actions.append(None)
            
        actions.append(EiderActions.Action("Properties...", lambda widget, filename, self=self: self.show_properties_dialog()))
        return EiderActions.create_menu(actions, self.filename)

    Icons = [
	("^Makefile","mc/makefile.xpm"),
	# C
	("\.c$","mc/gnome-application-x-c-source.png"),
	("\.h$","mc/gnome-application-x-c-header.png"),
	# C++ 
	("\.cc$","mc/gnome-application-x-cc-source.png"),
	("\.C$","mc/gnome-application-x-cc-source.png"),
	("\.cpp$","mc/gnome-application-x-cc-source.png"),
	# intermediate files
	("\.o$","mc/gnome-objectfile.png"),
	("\.a$","mc/gnome-library.png"),
	("\.so[0-9.]+$","mc/gnome-library.png"),
	# Fallback
	("","mc/gnome-textfile.png")
	]
    
    def get_base_icon_file(self):
        for icon in FileBase.Icons:
            if re.search(icon[0],self.filename):
                return EiderUtils.get_gnome_pixmap_file (icon[1])

    man_page_image = None

    def get_icon(self):
        file = self.get_base_icon_file()

        if file != None and os.path.isfile (file):
            base_icon = GdkImlib.Image(file)
            if not base_icon:
                print "Imlib failed to load %s" % file
                return None

            images = [base_icon]

            am_file = self.get_am_file()

            if isinstance(am_file,Automake.Source):
                images.append(GdkImlib.Image(EiderUtils.get_eider_pixmap_file ("source-symbol.png")))
            elif isinstance(am_file,Automake.Target):
                images.append(GdkImlib.Image(EiderUtils.get_eider_pixmap_file ("target-symbol.png")))

            return EiderImages.merge_images(images)
        else:
            print "no icon file known for this EiderFile"
            return None

    def show_properties_dialog (self):
        dialog = GnomePropertyBox()

        file = File (_base=self)
        pages = []

        page = DefaultPropertiesPage (file, dialog)
        pages.append (page)

        page = None
        mime = self.get_mime_type ()
        if mime != None:
            try:
                page = mime.get_property_page(file, dialog)
            except NotImplementedError:
                page = None
        if page != None:
            pages.append(page)

        page = None
        am_file = self.get_am_file()
        if am_file != None:
            try:
                page = am_file.get_property_page(file, dialog)
            except NotImplementedError:
                page = None
        if page != None:
            pages.append(page)


        for page in pages:
            dialog.append_page (page.get_widget(), GtkLabel (page.get_title()))

        def apply_cb (widget, page_num, pages=pages):
            if page_num != -1:
                pages[page_num].apply()

        dialog.connect ("apply", apply_cb)
        dialog.show()

    def ref(self):
        self.refcount = self.refcount + 1
        return self

    def unref(self):
        self.refcount = self.refcount - 1
        if (self.refcount == 0):
            del _files[self.filename]

class File:
    def __init__(self, filename = "", _base = None):
        if _base != None:
            self.base = _base
        else:
            if _files.has_key(filename):
                self.base = _files[filename].ref()
            else:
                self.base = FileBase(filename)

    def __del__(self):
	# We need the hasattr check, since we might get destroyed if the constructor fails
        if hasattr (self, 'base'):
		self.base.unref()

    def get_filename (self):
        return self.base.filename
        
    def get_basename (self):
        (dir,base) = os.path.split(self.base.filename)
        return base

    def get_mime_type (self):
        return self.base.get_mime_type ()

    def get_am_file (self):
        return self.base.get_am_file ()
        
    def get_am_file (self):
        return self.base.get_am_file()

    def get_popup_menu (self):
        "Returns a popup menu of actions for the file"
        return self.base.get_popup_menu()

    def show_properties_dialog (self):
        "Show the properties dialog for the file"
        self.base.show_properties_dialog()
    
    def get_icon(self):
        return self.base.get_icon()
        
class PropertiesPage:
    def __init__(self, file, property_box):
        raise NotImplementedError

    def get_widget (self):
        raise NotImplementedError

    def get_title (self):
        raise NotImplementedError

    def apply (self):
        raise NotImplementedError

class DefaultPropertiesPage (PropertiesPage): 
    def __init__(self, file, property_box):
        self.xml = libglade.GladeXML(EiderUtils.get_eider_glade_file("file-properties.glade"), "file-properties-page")
        self.basename = file.get_basename ()

        hbox = self.xml.get_widget ("file_properties_hbox")
        image = file.get_icon ()
        image.render ()
        icon = image.make_pixmap ()
        icon.show ()
        hbox.pack_start (icon)
        self.entry = self.xml.get_widget ("file_name_entry")
        self.entry.set_text (self.basename)
        label = self.xml.get_widget ("full_name_label")
        label.set_text (file.get_filename ())

        label = self.xml.get_widget ("file_type_label")
        text = ""
        mime = file.get_mime_type ()
        text = mime.get_mime_type ()
        if text == "":
            text = "<Unknown mime type>"
        label.set_text (text)

    def get_widget(self):
        return self.xml.get_widget ("file-properties-page")


    def get_title(self):
        return "Properties"

    def apply (self):
        pass
    
class GUIAMFile (Automake.AMFile):
    def __init__ (self, filename):
        self.filename = filename
    
    def get_property_page (self, file, dialog):
        "Return the property page object for this type of automake file"
        raise NotImplementedError
    
    def get_actions (self):
        """Return a list of EiderActions.Action objects that apply to the file type
           Derived classes should concatenate the actions from their parent class."""
        return []

class GUISource (Automake.Source, GUIAMFile):
    pass
    

# UI Utility functions for targets ... probably should be moved elsewhere

example_subs = {
	"prefix": "/usr",
	"exec_prefix": "/usr",
	"datadir": "/usr/share",
	"libdir": "/usr/lib",
	"libexecdir": "/usr/libexec",
	"bindir": "/usr/bin",
	"sbindir": "/usr/sbin",
	"sysconfdir": "/etc",
	"sharedstatedir": "/usr/com",
	"localstatedir": "/var",
	"pythondir": "/usr/lib/python/site-packages"
	}

sub_re = re.compile("\$\(([a-zA-Z_]+)\)")

def query_install_dir(makefile, message, allowed_sysdirs):
    installdirs = makefile.get_installdirs(allowed_sysdirs)
    
    xml = libglade.GladeXML(EiderUtils.get_eider_glade_file("eider-files.glade"), "choose-install-dialog")

    dialog = xml.get_widget ("choose-install-dialog")
    dialog.set_policy(0,1,0)
    dialog.set_default_size(-1,300)

    file_label = xml.get_widget ("choose-install-file-label")
    file_label.set_text (message)

    desc_label = xml.get_widget ("choose-install-desc-label")
    example_label = xml.get_widget ("choose-install-example-label")

    clist = xml.get_widget ("choose-install-clist")

    keys = installdirs.keys()
    keys.sort()

    for key in keys:
        (directory, description) = installdirs[key]

        if directory == None:
            directory = ""

        def sub_func(match):
            varname = match.group(1)
            if example_subs.has_key(varname):
                return example_subs[varname]
            else:
                return "$(" + varname + ")"

        example = sub_re.sub(sub_func, directory)

        row = clist.append([key, directory])
        clist.set_row_data (row, (example, description))

    def select_row_cb (clist, row, column, event, desc_label=desc_label, example_label=example_label):
        (example,description) = clist.get_row_data (row)
        desc_label.set_text(description)
        example_label.set_text(example)
        
    clist.connect ("select_row", select_row_cb)

    # Set label for initial row
    select_row_cb (clist, 0, 0, None)

    result = None
    if dialog.run() == 0:
        if len(clist.selection) > 0:
            result = clist.get_text(clist.selection[0], 0)
    
    dialog.destroy()
    return result
