import HookList
from IconList import *
import Eider

class FileOperations:
	def __init__ (self, parent, xml):
		# circular references here.  We should
		# move parent to be a SystemModule
		self.parent = parent
		xml.signal_autoconnect (
			{ "on_file_open_activate" : self.open_cb,
			  "on_file_properties_activate" : self.properties_cb,
			  "on_file_save_activate" : self.save_cb})

		self.toolbar_new = xml.get_widget("file_toolbar_new")
		self.toolbar_open = xml.get_widget("file_toolbar_open")
		self.toolbar_save = xml.get_widget("file_toolbar_save")
		self.toolbar_props = xml.get_widget("file_toolbar_properties")
		self.toolbar_print_file = xml.get_widget("file_toolbar_print_file")

		self.menu_new = xml.get_widget("file_menu_new")
		self.menu_open = xml.get_widget("file_menu_open")
		self.menu_save = xml.get_widget("file_menu_save")
		self.menu_props = xml.get_widget("file_menu_properties")
		self.menu_print_file = xml.get_widget("file_menu_print_file")

		IconList.selected_hook_list.add (self.file_selected_hook)
		IconList.unselected_hook_list.add (self.file_unselected_hook)
		editor = Eider.TextEditor.get()
		editor.get_file_changed_hook().add (self.file_changed_hook)
		editor.get_file_saved_hook().add (self.file_saved_hook)
		self.file_unselected_hook ()

	def file_selected_hook (self, file):
		dirtyfiles = Eider.TextEditor.get().get_dirty_files()

		if file.get_filename () in dirtyfiles:
			self.toolbar_save.set_sensitive (1)
			self.menu_save.set_sensitive (1)
		else:
			self.toolbar_save.set_sensitive (0)
			self.menu_save.set_sensitive (0)
		self.toolbar_open.set_sensitive (1)
		self.menu_open.set_sensitive (1)
		self.toolbar_props.set_sensitive (1)
		self.menu_props.set_sensitive (1)
		self.toolbar_print_file.set_sensitive (0)
		self.menu_print_file.set_sensitive (0)

	def file_unselected_hook (self):
		self.toolbar_open.set_sensitive (0)
		self.menu_open.set_sensitive (0)
		self.toolbar_save.set_sensitive (0)
		self.menu_save.set_sensitive (0)
		self.toolbar_props.set_sensitive (0)
		self.menu_props.set_sensitive (0)
		self.toolbar_print_file.set_sensitive (0)
		self.menu_print_file.set_sensitive (0)

	def file_changed_hook (self, file):
		icon_file = self.parent.icon_list.get_selected_icon()
		
		if icon_file != None and icon_file.get_filename() == file:
			self.toolbar_save.set_sensitive (1)

	def file_saved_hook (self, file):
		icon_file = self.parent.icon_list.get_selected_icon()
		if icon_file != None and icon_file.get_filename() == file:
			self.toolbar_save.set_sensitive (0)

	def open_cb(self,widget):
		file = self.parent.icon_list.get_selected_icon()
		if file != None:
			editor = Eider.TextEditor.get ()
			editor.open_file (file.get_filename ())

	def save_cb(self,widget):
		file = self.parent.icon_list.get_selected_icon()
		if file != None:
			editor = Eider.TextEditor.get ()
			editor.save_file (file.get_filename ())

	def properties_cb(self, widget):
		file = self.parent.icon_list.get_selected_icon()
		if file != None:
			file.show_properties_dialog()
