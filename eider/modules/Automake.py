import sys
import os
import re
import string
import HookList

ParseError = 'Parse Error'
IsConditional = 'Attempt to modify conditionally set value'

primaries = {}

sys_installdirs = { "pgkdata":
		    ("$(datadir)/$(pkgname)",
		     "Directory for package data files"),
		    "pgklib":
		    ("$(libdir)/$(pkgname)",
		     "Directory for package library files"),
		    "libexec":
		    ("$(libexecdir)",
		     "Directory for executables that other programs run"),
		    "bin": 
		    ("$(bindir)",
		     "Directory for executables that users run"),
		    "sbin":
		    ("$(sbindir)",
		     "Directory for executables that system administrators run"),
		    "sysconf":
		    ("$(sysconfdir)",
		     "Directory for read-only configuration data"),
		    "sharedstate":
		    ("$(sharedstate)",
		     "Directory for modifiable data shared between machines"),
		    "localstate":
		    ("$(localstatedir)",
		     "Directory for modifiable data for one machine"),
		    "python": 
		    ("$(pythondir)",
		     "Directory for python modules"),
		     "noinst":
		    (None,
		     "Uninstalled file"),
		     "EXTRA":
		    (None,
		     "File that is optionally built"),
		    "check":
		    (None,
		     "Executable used only during \"make check\"")
		    }

def register_primary(primary_name, target_class):
	if primaries.has_key(primary_name):
		raise Error, "Duplicate definition of primary: " + primary_name + "\n"
	primaries[primary_name] = target_class

class AMFile:
	def set_filename(self, filename):
		"Change the name of the file"
		raise NotImplementedError
	
	def get_basename(self):
		(dir,base) = os.path.split(self.filename)
		return base
	
	def get_dirname(self):
		(dir,base) = os.path.split(self.filename)
		return dir
	
class Source (AMFile):
	def __init__(self, filename, target):
		self.filename = filename
		self.target = target

	def set_filename(self, filename):
		(olddir,oldbase) = os.path.split (self.filename)
		(newdir,newbase) = os.path.split (filename)
		if olddir != newdir:
			raise ValueError, "Cannot change source's directory"

		self.filename = filename
		self.target.source_name_changed (oldbase, newbase)

class Target (AMFile):
	def __init__(self, filename, installdir, makefile):
		self.filename = filename
		self.installdir = installdir
		self.makefile = makefile

	def get_primary(self):
		raise NotImplementedError

	def set_variable(self, key, value):
		raise NotImplementedError

	def source_name_changed(self, oldname, newname):
		raise NotImplementedError

	def set_filename(self, filename):
		(olddir,oldbase) = os.path.split (self.filename)
		(newdir,newbase) = os.path.split (filename)
		if olddir != newdir:
			raise ValueError, "Cannot change target's directory"

		varname = self.installdir + "_" + self.get_primary()
		files = string.split(self.makefile.get_variable(varname))
		try:
			index = files.index(oldbase);
		except ValueError:
			raise ValueError, "Attempt to rename target set in conditional or expanded variable"
		del files[index]
		files.append(newbase)
		
		self.makefile.set_variable(varname, string.join(files))

	canonicalize_re = re.compile("[^A-Za-z0-9]")
	
	def get_canonical_name(self):
		return Target.canonicalize_re.sub("_", self.get_basename())
		
class Makefile:
	def __init__(self,dir):
		self.dir = dir
		self.filename = os.path.join (self.dir, "Makefile.am")
		self.contents = []
		self.variables = {}
		self.installdirs = {}
		self.have_backup = 0
		self.changed_hook = HookList.HookList("Hook to run when makefile contents change")

	variable_re = re.compile("\s*([0-9A-Za-z_]+)\s*=(.*)", re.DOTALL)
	subst_re = re.compile("\$\(([0-9A-Za-z_]+)\)")
	keyword_re = re.compile("(if|else|endif)(\s+(.*))?$")

	def substitute_variables(self, value, scopes=None):
		if scopes == None:
			scopes=[ self.variables ]
		return Makefile.subst_re.sub(
			lambda match, makefile=self, scopes=scopes: \
				makefile.get_variable(match.group(1), 1, scopes)
			,value)

	def get_variable(self, variable, substitute = 0, scopes = None):
		if scopes == None:
			scopes=[ self.variables ]
		
		for scope in scopes:
			if (scope.has_key(variable)):
				if substitute:
					return self.substitute_variables (scope[variable][0])
				else:
					return scope[variable][0]
		return ""

	def get_installdirs(self, allowed_sysdirs=[]):
		result = self.installdirs
		for (key,value) in sys_installdirs.items():
			if key in allowed_sysdirs:
				result[key] = value
		return result

	def _parse_error(self, message):
		raise ParseError, (self.filename, self.lineno, message)


	def _merge_strings(self, value1, value2):
		values1 = string.split(value1)
		values2 = string.split(value2)
		results = values1 + []

		map = {}
		for value in values1:
			map[value] = 1

		for value in values2:
			if not map.has_key(value):
				results.append(value)

		return string.join(results)

	def _merge_subscopes(self, scope, scope1, scope2):
		for (key, value) in scope1.items():
			if scope2.has_key(key):
				scope[key] = (self._merge_strings(value[0], scope2[key][0]), -1)
			else:
			        scope[key] = (scope1[key][0], -1)

		for (key, value) in scope2.items():
			if not scope1.has_key(key):
				scope[key] = (scope2[key][0], scope2[key][1], -1)
	
	def parse_recurse(self, file, scopes):
		while 1:
			line = file.readline()
			if not line:
				return None
			self.lineno = self.lineno + 1

			while len(line) > 1 and line[-2] == '\\' and line[-1] == '\n':
				continuation = file.readline()
				if not continuation:
					self._parse_error("File cannot end with continuation line")
				self.lineno = self.lineno + 1
				line = line[:-2] + "\n" + continuation

			if line[-1] == '\n':
				line = line[:-1]

			self.contents.append(line)

			match = Makefile.keyword_re.match(line)
			if match:
				keyword = match.group(1)

				if keyword == "if":
					if match.group(3) == None or not re.match("[0-9A-Za-z_]+$", match.group(3)):
						self._parse_error ("Cannot parse if statement '" + line + "'")

					subscope1 = {}
					subscope2 = {}
					end_keyword = self.parse_recurse(file, [ subscope1 ] + scopes)
					if end_keyword == "else":
						end_keyword = self.parse_recurse(file, [ subscope2 ] + scopes)
						if end_keyword != "endif":
							self._parse_error("Missing endif statement")
					elif end_keyword != "endif":
						self._parse_error("Missing endif statement")

					self._merge_subscopes (scopes[0], subscope1, subscope2)
						
				elif keyword == "else":
					if match.group(3) != None:
						self._parse_error ("Invalid else statement '" + line + "'")
					return keyword
				else: # endif
					if match.group(3) != None:
						self._parse_error ("Invalid endif statement '" + line + "'")
					return keyword
				

			match = Makefile.variable_re.match(line)
			if match:
				scopes[0][match.group(1)] = (match.group(2), len(self.contents) - 1)
				
	def parse(self):
		file = open(self.filename)
		self.lineno = 0

		self.parse_recurse(file,[self.variables])

		file.close()

	def set_variable(self,variable,new_value):
		if (self.variables.has_key(variable)):
			tuple = self.variables[variable]
			if (tuple[1] == -1):
				raise IsConditional, variable
			self.contents[tuple[1]] = variable + " = " + new_value
			self.variables[variable] = (new_value, tuple[1])
		else:
			self.contents.append(variable + " = " + new_value)
			self.variables[variable] = (new_value, len(self.contents) - 1)

	def unset_variable(self,variable):
		if (self.variables.has_key(variable)):
			tuple = self.variables[variable]
			if (tuple[1] == -1):
				raise IsConditional, variable

			del self.contents[tuple[1]]
			del self.variables[variable]

	def register_source(self,name,source):
		self.sources[name] = source

	def find_files(self):
		self.installdirs = {}

		# Locate all installation directories
		for (key,value) in self.variables.items():
			if (key[-3:] == "dir"):
				self.installdirs[key[:-3]] = (value[0],"")

		var_re = re.compile("([0-9A-Za-z_]+)_([0-9A-Za-z]+)$")

		self.targets = {}
		self.sources = {}
		self.extra_dist = ""

		# Find all targets
		for (key,value) in self.variables.items():
			# Special cases

			if key == "EXTRA_DIST":
				self.extra_dist = value
				continue
			
			match = var_re.match (key)
			
			if match and (self.installdirs.has_key(match.group(1)) or sys_installdirs.has_key(match.group(1))):
				primary = match.group(2)
				if not primaries.has_key(primary):
					sys.stderr.write("Variable '%s' key might refer to an unknown primary. You may want to rename it\n" % key)
					continue

				target_names = string.split(value[0])
				for name in target_names:
					self.targets[name] = primaries[primary](os.path.join(self.dir,name), match.group(1), self)

		# Set variables for each target

		# sort targets in descending order of length so that we always find
		# the longest matching prefix.
		target_names = self.targets.keys()

		def sort_func (a,b):
			if len(a) < len(b):
				return 1
			elif len(a) > len(b):
				return -1
			else:
				return 0
		
		target_names.sort (sort_func)

		# FIXME: This is likely to be _very_ slow - the get_canonical_name at least
		# needs to be pulled out
		for (key,value) in self.variables.items():
			for target_name in target_names:
				target = self.targets[target_name]
				prefix = target.get_canonical_name() + "_"
				prefix_len = len(prefix)
				if key[:prefix_len] == prefix:
					target.set_variable(key[prefix_len:], value[0])
					break
		
	def write(self):
		line_re = re.compile("\n")
		
		if not self.have_backup:
			if os.system("mv %s %s" % (self.filename, self.filename + ".bak")) != 0:
				raise RuntimeError, "Cannot make backup file"
			self.have_backup = 1

		file = open(self.filename, "w")
		for line in self.contents:
			line = line_re.sub("\\\n", line)
			file.write(line + "\n")
		file.close()

def main():
	if len(sys.argv) != 2:
		sys.stderr.write("Usage: automake.py Makefile.am\n")
		sys.exit(1)

	makefile = Makefile(sys.argv[1])
	makefile.parse()

	for (key,value) in makefile.variables.items():
		print key, ":", value, "-", makefile.substitute_variables(value[0])

	makefile.set_variable("bin_PROGRAMS", "captive-server.el")
	makefile.set_variable("EXTRA_DIST", "captive-server.el")

	makefile.find_files()

	print "TARGETS"
	print makefile.targets
	for (key, value) in makefile.targets.items():
		print key,":",value
	
	print "SOURCES"
	for (key, value) in makefile.sources.items():
		print key,":",value
	
	makefile.write(sys.argv[1] + ".munged")

if __name__ == "__main__":
	main()
