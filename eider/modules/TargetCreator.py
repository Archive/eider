class TargetCreator:
        def __init__(self):
                raise NotImplementedError

        def get_druid(self, dest_druid):
                raise NotImplementedError

        def get_preference_window (self):
                raise NotImplementedError

	def create_target(self):
		raise NotImplementedError


