(setq debug-on-error 't)

(setq captive-output "")
(setq captive-sequence 0)
(setq captive-pending '())

(mapcar (lambda (a) nil) '(1 3 4))

(defun captive-alist-delete (alist seqno)
  (if alist
      (if (= (car (car alist)) seqno)
	  (captive-alist-delete (cdr alist) seqno)
	(cons (car alist) (captive-alist-delete (cdr alist) seqno)))
    '()))

(defun captive-get-output (seqno)
  (let ((result (cdr (assoc seqno captive-pending))))
    (setq captive-pending (captive-alist-delete captive-pending seqno))
    result))
		
(defun captive-set-output (seqno output)
  (setq captive-pending
	(mapcar (lambda (pair)
		  (if (= seqno (car pair))
		      (cons seqno output)
		    pair)) captive-pending)))


(defun captive-process-filter (process output)
  (setq captive-output (concat captive-output output))
  (while (string-match "\\([^\0]*\\)\0" captive-output)
    (let ((command (match-string 1 captive-output)))
      (setq captive-output
	    (substring captive-output (match-end 0)))
      (cond ((string-match "EVAL[ \t]*\\([0-9]*\\)[ \t]*\n" command)
	     (let ((text (substring command (match-end 0)))
		   (seqno (match-string 1 command)))
	       (process-send-string captive-process
				    (condition-case err
					(concat "RESULT " seqno "\n"
						(prin1-to-string (eval (read text)))
						[0])
				      (error (concat "ERROR " seqno "\n"
						     (prin1-to-string err) [0]))))))
	    ((string-match "RESULT[ \t]*\\([0-9]*\\)[ \t]*\n" command)
	     (captive-set-output (string-to-number (match-string 1 command))
				 (substring command (match-end 0))))))))

(defun captive-send (output wait)
  (let ((seqno captive-sequence))
    (setq captive-sequence (+ captive-sequence 1))
    (process-send-string captive-process
			 (concat "EVAL " (number-to-string seqno) "\n"
				 output [0]))
    (if wait
	(progn
	  (setq captive-pending (cons (cons seqno nil) captive-pending))
	  (catch 'result
	    (while t
	      (accept-process-output captive-process)
	      (let ((res (captive-get-output seqno)))
		(if res
		    (throw 'result res)))))))))
	

(defun captive-start (server infd outfd)
  (setq captive-process
	(let ((process-connection-type nil))  ; Use a pipe.
	  (start-process "Captive" nil server
			 (number-to-string infd)
			 (number-to-string outfd))))
  (set-process-filter captive-process 'captive-process-filter))

;;; 
;;; The following functions are handy, but not directly related
;;; to the function of capturing
;;;


(defun captive-filter (predicate seq)
  "Return elements of seq matching predicate"
  (cond ((null seq) '())
	((funcall predicate (car seq))
	 (cons (car seq)
	       (captive-filter predicate (cdr seq))))
	(t (captive-filter predicate (cdr seq)))))

(defun captive-get-dirty-files ()
  (mapcar (lambda (buffer) (buffer-file-name buffer))
	  (captive-filter
	   (lambda (buffer) (and (buffer-modified-p buffer)
				 (not (buffer-base-buffer buffer))
				 (buffer-file-name buffer)))
	   (buffer-list))))

  
(defun file-changed-hook ()
  "Hook to notify eider when a file is changed"
  (let ((string (format "run_file_changed_hook (\"%s\")"
			buffer-file-name)))
    (captive-send string 0)))

(defun file-saved-hook ()
  "Hook to notify eider when a file is changed"
  (let ((string (format "run_file_saved_hook (\"%s\")"
			buffer-file-name)))
    (captive-send string 0)))

(add-hook 'first-change-hook 'file-changed-hook 1)
(add-hook 'after-save-hook 'file-saved-hook 1)
