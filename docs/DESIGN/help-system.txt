Help System
===========

The components of the help system are:

 - A browser for API reference docs (and maybe section 2 man pages)
   (Tree view plus HTML widget)

 - Context sensitive help on GNOME and GTK+ functions (gnome-tags)

 - Indexing and searching functionality (future addition, with GNOME 
   2.0 help system most likely, though something simple based on
   gnome-tags should be quite straightforward.)


